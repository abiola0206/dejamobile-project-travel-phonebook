package dejamobile.project.TravelPhoneBook.model.DTO;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


public class VileDTO {

    private long vileId;

    @NotNull
    private String vileName;

    private long departmentId;

    @NotNull
    private RegionDTO vileRegion;

    @NotNull
    private List<SportDTO> sportDTOS = new ArrayList<>();

    public long getVileId() {
        return vileId;
    }

    public void setVileId(long vileId) {
        this.vileId = vileId;
    }

    public String getVileName() {
        return vileName;
    }

    public void setVileName(String vileName) {
        this.vileName = vileName;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }

    public RegionDTO getVileRegion() {
        return vileRegion;
    }

    public void setVileRegion(RegionDTO vileRegion) {
        this.vileRegion = vileRegion;
    }

    public List<SportDTO> getSportDTOS() {
        return sportDTOS;
    }

    public void setSportDTOS(List<SportDTO> sportDTOS) {
        this.sportDTOS = sportDTOS;
    }
}

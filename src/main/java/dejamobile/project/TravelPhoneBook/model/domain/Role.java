package dejamobile.project.TravelPhoneBook.model.domain;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name = "ROLES")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private RoleName rolename;

    public Role() {}

    public Role(RoleName rolename) {
        this.rolename = rolename;
    }

    public RoleName getRolename() {
        return rolename;
    }

    public void setRolename(RoleName roleName) {
        this.rolename = roleName;
    }
}

package dejamobile.project.TravelPhoneBook.model.domain;


import io.swagger.annotations.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "VILE")
@ApiModel(description = "All details about the Viles. ")
public class Vile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "VILE_ID")
    @ApiModelProperty(notes = "The database generated vile ID")
    private long vileId;

    @Column(name = "VILE_NAME", nullable = false, length = 100)
    @ApiModelProperty(notes = "The database generated vile's name")
    private String vileName;


    @ApiModelProperty(notes = "The database generated the department for each vile")
    @Access(AccessType.PROPERTY)
    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "DEPARTMENT_ID")
    private Department department;


    @ApiModelProperty(notes = "The database generated the region for each vile")
    @Access(AccessType.PROPERTY)
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "REGION_ID")
    private Region region;


    /**
     *  each city will have a unique sport and whether, this
     *  is why am using HashSet and not list
     */
    @ApiModelProperty(notes = "The database generated ville and sport table")
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "VILE_SPORT", joinColumns = { @JoinColumn(name = "VILE_ID") }, inverseJoinColumns = { @JoinColumn(name = "SPORT_ID") })
    private Set<Sport> sports = new HashSet<Sport>(0);



    public Vile() {
        super();
    }


    public Vile(String vileName, Department department, Region region) {
        this.vileName = vileName;
        this.department = department;
        this.region = region;
    }

    public Vile(String vileName, Department department, Region region, Set<Sport> sports) {
        this.vileName = vileName;
        this.department = department;
        this.region = region;
        this.sports = sports;
    }


    public long getVileId() {
        return vileId;
    }

    public void setVileId(long vileId) {
        this.vileId = vileId;
    }

    public String getVileName() {
        return vileName;
    }

    public void setVileName(String villeName) {
        this.vileName = villeName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Set<Sport> getSports() {
        return sports;
    }

    public void setSports(Set<Sport> sports) {
        this.sports = sports;
    }


    @Override
    public String toString() {
        return "model.domain.Vile [id=" + vileId + ", name=" + vileName + ", department="
                + department.getDepartmentName() + "]";
    }
}

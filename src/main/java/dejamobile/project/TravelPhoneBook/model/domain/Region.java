package dejamobile.project.TravelPhoneBook.model.domain;


import io.swagger.annotations.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REGION")
@ApiModel(description = "All details about the Region. ")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REGION_ID")
    @ApiModelProperty(notes = "The database generated region ID")
    private long regionId;

    @Column(name = "REGION_NAME", nullable = false, length = 100)
    @ApiModelProperty(notes = "The database generated region ID")
    private String regionName;


    @ApiModelProperty(notes = "The database generated department for each region")
    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "region", cascade= CascadeType.ALL)
    private List<Department> departments = new ArrayList<Department>();

    @ApiModelProperty(notes = "The database generated ville for each region")
    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "region", cascade=CascadeType.ALL)
    private List<Vile> viles = new ArrayList<Vile>();

    public Region() {
        super();
    }

    public Region(String regionName) {
        this.regionName = regionName;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public List<Vile> getViles() {
        return viles;
    }

    public void setViles(List<Vile> viles) {
        this.viles = viles;
    }

    @Override
    public String toString() {
        return "model.domain.Region [id=" + regionId + ", name=" + regionName + ", departments="
                + departments  + "]";
    }
}

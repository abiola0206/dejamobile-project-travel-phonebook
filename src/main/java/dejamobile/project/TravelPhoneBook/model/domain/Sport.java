package dejamobile.project.TravelPhoneBook.model.domain;


import io.swagger.annotations.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SPORT")
@ApiModel(description = "All details about the Viles. ")
public class Sport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="SPORT_ID")
    private long sportId;

    @Column(name="SPORT_NAME", nullable=false)
    private String sportName;

    @ManyToMany(mappedBy = "sports", cascade= CascadeType.ALL )
    private List<Vile> viles = new ArrayList<Vile>();

    public Sport() {
    }

    public Sport(String sportName) {
        this.sportName = sportName;
    }

    public long getSportId() {
        return sportId;
    }

    public void setSportId(long sportId) {
        this.sportId = sportId;
    }

    public String getSportName() {
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

    public List<Vile> getViles() {
        return viles;
    }

    public void setViles(List<Vile> viles) {
        this.viles = viles;
    }
}

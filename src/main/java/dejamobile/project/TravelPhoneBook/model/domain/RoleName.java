package dejamobile.project.TravelPhoneBook.model.domain;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}

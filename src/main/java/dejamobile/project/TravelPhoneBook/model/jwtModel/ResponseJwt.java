package dejamobile.project.TravelPhoneBook.model.jwtModel;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class ResponseJwt {

    private String token;
    private String type = "Bearer";
    private String userName;
    private final Collection<? extends GrantedAuthority> authorities;

    public ResponseJwt(String token, String userName,
                       Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.userName = userName;
        this.authorities = authorities;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

}

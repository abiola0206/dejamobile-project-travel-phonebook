package dejamobile.project.TravelPhoneBook.mappers;

import dejamobile.project.TravelPhoneBook.model.DTO.VileDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Vile;
import org.mapstruct.*;

import java.util.List;


@Mapper(componentModel = "spring", uses = {DepartmentMapper.class})
public interface VileMapper extends  IEntityMapper<VileDTO, Vile> {


    @Mappings({
            @Mapping(source = "region", target = "vileRegion"),
            @Mapping(source = "department.departmentId", target = "departmentId"),
            @Mapping(source = "sports", target = "sportDTOS")
    })
        // @Mapping(source = "department.departmentId", target = "departmentId")
    VileDTO toDto(final Vile vile);

    List<VileDTO> toDto(final List<Vile> viles);

    @Mappings({
            @Mapping(source = "vileRegion", target = "region"),
            @Mapping(source = "departmentId", target = "department"),
            @Mapping(source = "sportDTOS", target = "sports")
    })
        //@Mapping(source = "departmentId", target = "department")
    Vile toDomain(final VileDTO vileDTO);

    List<Vile> toDomain(final List<VileDTO> vileDTOS);

    default Vile fromId(final Long vileId){
        if (vileId == null){
            return null;
        }

        final Vile vile = new Vile();
        vile.setVileId(vileId);

        return vile;
    }
}

package dejamobile.project.TravelPhoneBook.mappers;


import dejamobile.project.TravelPhoneBook.model.DTO.SportDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Sport;
import org.mapstruct.*;

import java.util.List;


@Mapper(componentModel = "spring", uses = {VileMapper.class})
public interface SportMapper {

    @Mapping(source = "viles", target = "vileDTOs")
    SportDTO toSportDTO(Sport sport);

    List<SportDTO> toSportDTOs(List<Sport> sports);

    Sport toSport(SportDTO sportDTO);

}

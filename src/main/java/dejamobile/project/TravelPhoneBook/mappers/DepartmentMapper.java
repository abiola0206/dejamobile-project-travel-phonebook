package dejamobile.project.TravelPhoneBook.mappers;

import dejamobile.project.TravelPhoneBook.model.DTO.DepartmentDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Department;
import org.mapstruct.*;
import org.springframework.context.annotation.Bean;


@Mapper(componentModel = "spring", uses = {VileMapper.class})
public interface DepartmentMapper extends IEntityMapper<DepartmentDTO, Department> {

    @Mappings({
            @Mapping(source = "viles", target = "vileDTOs"),
            @Mapping(source = "region", target = "depRegion")
    })
        //@Mapping(source = "viles", target = "vileDTOs")
    DepartmentDTO toDto(final Department department);

    Department toDomain(final DepartmentDTO departmentDTO);

    default Department fromId(final Long departmentId){
        if (departmentId == null){
            return  null;
        }

        final Department department = new Department();
        department.setDepartmentId(departmentId);

        return department;
    }
}

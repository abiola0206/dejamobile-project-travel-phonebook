package dejamobile.project.TravelPhoneBook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@SpringBootApplication
@EnableJpaRepositories
@EnableTransactionManagement
@EntityScan

/**
 * @EnableAutoConfiguration @ComponentScan(basePackages={""}) @EnableJpaRepositories(basePackages="") @EnableTransactionManagement @EntityScan(basePackages="")
 */
public class TravelPhoneBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelPhoneBookApplication.class, args);
	}

}

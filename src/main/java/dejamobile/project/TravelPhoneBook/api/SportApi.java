package dejamobile.project.TravelPhoneBook.api;


import dejamobile.project.TravelPhoneBook.mappers.SportMapper;
import dejamobile.project.TravelPhoneBook.model.DTO.SportDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Sport;
import dejamobile.project.TravelPhoneBook.service.SService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;



import java.util.List;
import java.util.Optional;
import javax.validation.Valid;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/api/info/phoneBook", produces = MediaType.APPLICATION_JSON_VALUE)
@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
@Api(value = "Sport Management System", description = "Operations pertaining to sport in Sport Management System")
public class SportApi {

    private final SService sService;

    private final SportMapper sportMapper;

    @Autowired

    public SportApi(SService sService, SportMapper sportMapper) {
        this.sService = sService;
        this.sportMapper = sportMapper;
    }

    /**
     *
     * @return the list of sports
     */
    @GetMapping(path="/sport")
    public ResponseEntity<List<SportDTO>> findAll() {
        return ResponseEntity.ok(sportMapper.toSportDTOs(sService.findAll()));
    }


    /**
     *
     * @param sportId
     * @return a sport with a particular Id
     */
    @GetMapping(path="/sport/{sportId}")
    public ResponseEntity<SportDTO> findById(@PathVariable(value = "sportId") Long sportId) {
        Optional<Sport> sport = sService.findById(sportId);

        return ResponseEntity.ok(sportMapper.toSportDTO(sport.get()));
    }


    /**
     *
     * @param sportDTO
     * @return the items posted in a new sport
     */
    @PostMapping(path = "/sport", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SportDTO> create(@RequestBody SportDTO sportDTO) {
        sService.save(sportMapper.toSport(sportDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(sportDTO);
    }



    /**
     *
     * @param sportId
     * @param sportDetails
     * @return a modified sport
     */
    @PutMapping(path = "/sport/{sportId}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SportDTO> updateSport(@PathVariable(value = "sportId") Long sportId,
                                                @Valid @RequestBody SportDTO sportDetails) {
        Sport sport = sportMapper.toSport(sportDetails);
        sport.setSportId(sportId);

        sService.save(sport);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(sportDetails);
    }


    /**
     *
     * @param sportId
     * @return nothing
     */
    @DeleteMapping("/sport/{sportId}")
    public ResponseEntity delete(@PathVariable(value = "sportId") Long sportId) {
        sService.deleteById(sportId);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }


}

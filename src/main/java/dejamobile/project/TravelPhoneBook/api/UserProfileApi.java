package dejamobile.project.TravelPhoneBook.api;


import dejamobile.project.TravelPhoneBook.exception.ResourceNotFoundException;
import dejamobile.project.TravelPhoneBook.model.domain.UserProfile;
import dejamobile.project.TravelPhoneBook.model.domain.Users;
import dejamobile.project.TravelPhoneBook.repository.UserProfileRepository;
import dejamobile.project.TravelPhoneBook.service.MailService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/info/phoneBook", produces = MediaType.APPLICATION_JSON_VALUE)
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
@Api(value = "UserProfile Management System", description = "Operations pertaining to a user's profile in the application")
public class UserProfileApi {

    @Autowired
    UserProfileRepository userProfileRepository;

    @Autowired
    private MailService notificationService;

    @GetMapping(value = "/userProfileId/{id}")
    public UserProfile getUserProfileById(@PathVariable("id") int id)
    {
        UserProfile userProfile = userProfileRepository.findById(id).get();

        return userProfile;
    }

    @GetMapping(value = "/userProfile")
    public ResponseEntity<List<UserProfile>> findAllUsersProfile(){
        return ResponseEntity.ok(userProfileRepository.findAll());
    }


    @GetMapping(path="/userProfile/{username}")
    public ResponseEntity<UserProfile> findByUserName(@PathVariable(value = "username") String username) {

        UserProfile uProfile = userProfileRepository.findByUserName(username);
        return ResponseEntity.ok().body(uProfile);
    }

    @PostMapping(path = "/userProfile/{username}", consumes = "application/json", produces = "application/json")
    public UserProfile createProfile(@PathVariable(value = "username") String username,
                                     @RequestBody UserProfile userProfile) throws MessagingException {
        Users users = userProfileRepository.getUsers(username);
        userProfile.setUsers(users);

        userProfileRepository.save(userProfile);

        String receiversMail;
        receiversMail = users.getEmail();

        String mailBody;
        mailBody = "Hello " + users.getName() +
                " Find below the details for your travel trip in your phoneBook : " +
                "the region you went to " + userProfile.getUserRegion() + " "
                + "the department you went to " + userProfile.getUserDepartment() + " "
                + "the vile you went to " + userProfile.getUserVile() + " "
                + "the sport you did " + userProfile.getUserSport() + " "
                + "the pets you liked " + userProfile.getUserPet() + " "
                + "and the food you ate " + userProfile.getUserFood() + ".";

        notificationService.send(receiversMail, "Your Travel Trip Details", mailBody);

        return getUserProfileById((int) userProfile.getId());
    }


    @PutMapping(path ="/userProfile/{profileId}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UserProfile> updateProfile(@PathVariable(value = "profileId") int profileId,
                                                     @Valid @RequestBody UserProfile profileDetails) throws ResourceNotFoundException {
        UserProfile uProfile = userProfileRepository.findById(profileId)
                .orElseThrow(() -> new ResourceNotFoundException("UserProfile not found for this id :: " + profileId));

        uProfile.setUserRegion(profileDetails.getUserRegion());
        uProfile.setUserDepartment(profileDetails.getUserDepartment());
        uProfile.setUserVile(profileDetails.getUserVile());
        uProfile.setUserSport(profileDetails.getUserSport());
        uProfile.setUserPet(profileDetails.getUserPet());
        uProfile.setUserFood(profileDetails.getUserFood());
        final UserProfile updatedProfile = userProfileRepository.save(uProfile);

        return ResponseEntity.ok(updatedProfile);
    }

    @DeleteMapping("/userProfile/{id}")
    public Map<String, Boolean> deleteProfile(@PathVariable(value = "id") int id) {
        userProfileRepository.deleteByProfilesId(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

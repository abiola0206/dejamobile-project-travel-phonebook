package dejamobile.project.TravelPhoneBook.api;


import dejamobile.project.TravelPhoneBook.exception.ResourceNotFoundException;
import dejamobile.project.TravelPhoneBook.mappers.VileMapper;
import dejamobile.project.TravelPhoneBook.model.DTO.VileDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Vile;
import dejamobile.project.TravelPhoneBook.service.VService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;



import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/api/info/phoneBook", produces = MediaType.APPLICATION_JSON_VALUE)
@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
@Api(value = "Vile Management System", description = "Operations pertaining to vile in Vile Management System")
public class VileApi {

    private final VService vService;
    private final VileMapper vileMapper;

    @Autowired
    public VileApi(VService vService, VileMapper vileMapper) {
        this.vService = vService;
        this.vileMapper = vileMapper;
    }

    /**
     *
     * @return the list of viles
     */
    @GetMapping(path="/vile")
    public ResponseEntity<List<VileDTO>> findAll(){
        return ResponseEntity.ok(vileMapper.toDto(vService.findAll()));
    }


    /**
     *
     * @param vileId
     * @return a ville with a particular Id
     * @throws ResourceNotFoundException
     */
    @GetMapping(path="/vile/{vileId}")
    public ResponseEntity<VileDTO> findById(@PathVariable(value = "vileId") Long vileId)
            throws ResourceNotFoundException {
        Optional<Vile> vile = Optional.ofNullable(vService.findById(vileId)
                .orElseThrow(() -> new ResourceNotFoundException("Vile not found for this id :: " + vileId)));
        return ResponseEntity.ok(vileMapper.toDto(vile.get()));
    }


    /**
     *
     * @param vileDTO
     * @return the items posted in a new ville
     */
    @PostMapping(path = "/vile", consumes = "application/json", produces = "application/json")
    public  ResponseEntity<VileDTO> create( @Valid @RequestBody VileDTO vileDTO)
    {
        vService.save(vileDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(vileDTO);
    }


    /**
     *
     * @param vileId
     * @param vileDetails
     * @return a modified vile
     */
    @PutMapping(path = "/vile/{vileId}", consumes = "application/json", produces = "application/json")
    public  ResponseEntity<VileDTO> updateVile(@PathVariable(value = "vileId") Long vileId,
                                                 @Valid @RequestBody VileDTO vileDetails)
    {
        Vile vile = vileMapper.toDomain(vileDetails);
        vile.setVileId(vileId);
        vService.save(vileDetails);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(vileDetails);

    }


    /**
     *
     * @param vileId
     * @return nothing
     */
    @DeleteMapping("/vile/{vileId}")
    public ResponseEntity delete(@PathVariable(value = "vileId") Long vileId){
        vService.deleteById(vileId);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

}

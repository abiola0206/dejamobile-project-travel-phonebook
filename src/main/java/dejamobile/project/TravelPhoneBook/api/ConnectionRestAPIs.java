package dejamobile.project.TravelPhoneBook.api;

import dejamobile.project.TravelPhoneBook.repository.UsersRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Api(value = "Connection Management System", description = "Operations pertaining to users connected to the application")
public class ConnectionRestAPIs {

    @Autowired
    UsersRepository usersRepository;

    @GetMapping("/api/phoneBook/hello")
    public String helloWord() {
        return "helle world";
    }

    @GetMapping("/api/info/phoneBook/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String userAccess() {
       return ">>> User Contents!";
    }

    @GetMapping("/api/info/phoneBook/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return ">>> Admin Contents!";
    }

    @GetMapping(value = "/api/info/phoneBook/registerUser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<?>> findAllUsers() {
       return ResponseEntity.ok(usersRepository.findAll());
    }
}

package dejamobile.project.TravelPhoneBook.repository;



import dejamobile.project.TravelPhoneBook.model.domain.UserProfile;
import dejamobile.project.TravelPhoneBook.model.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Integer> {

    @Query("select u from UserProfile u where u.users.username = :username")
    UserProfile findByUserName(@Param("username") String username);

    @Query("select p from Users p where p.username = :username")
    Users getUsers(String username);

    @Transactional
    @Modifying
    @Query("delete from UserProfile h where h.id = :id")
    void deleteByProfilesId(@Param("id") int id);
}

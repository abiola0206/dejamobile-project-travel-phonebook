package dejamobile.project.TravelPhoneBook.repository;



import dejamobile.project.TravelPhoneBook.model.domain.Role;
import dejamobile.project.TravelPhoneBook.model.domain.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    @Query("select r from Role as r where r.rolename = :rolename")
    Role findByName(@Param("rolename") RoleName rolename);
}

package dejamobile.project.TravelPhoneBook.repository;


import dejamobile.project.TravelPhoneBook.model.domain.Vile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VileRepository extends JpaRepository<Vile, Long> {
}

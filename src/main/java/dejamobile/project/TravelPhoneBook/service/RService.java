package dejamobile.project.TravelPhoneBook.service;


import dejamobile.project.TravelPhoneBook.mappers.DepartmentMapper;
import dejamobile.project.TravelPhoneBook.mappers.RegionMapper;
import dejamobile.project.TravelPhoneBook.model.DTO.RegionDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Department;
import dejamobile.project.TravelPhoneBook.model.domain.Region;
import dejamobile.project.TravelPhoneBook.repository.DepartmentRepository;
import dejamobile.project.TravelPhoneBook.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class RService {

    private final DepartmentRepository departmentRepository;
    private final RegionRepository regionRepository;
    private final DepartmentMapper departmentMapper;
    private final RegionMapper regionMapper;

    @Autowired

    public RService(DepartmentRepository departmentRepository, RegionRepository regionRepository,
                    DepartmentMapper departmentMapper, RegionMapper regionMapper) {
        this.departmentRepository = departmentRepository;
        this.regionRepository = regionRepository;
        this.departmentMapper = departmentMapper;
        this.regionMapper = regionMapper;
    }


    /**
     * to create a new region
     */
    public RegionDTO save(RegionDTO regionDTO){
        Region region = regionMapper.toDomain(regionDTO);

        Region createdRegion = regionRepository.save(region);
        List<Department> departments = new ArrayList<>();

        regionDTO.getDepartmentDTOS().forEach( departmentDTO -> {
            Department department = departmentMapper.toDomain(departmentDTO);
            department.setRegion(createdRegion);
            departments.add(department);
        });

        createdRegion.setDepartments(departments);
        departmentRepository.saveAll(departments);

        return regionMapper.toDto(createdRegion);
    }



    /**
     * to get list of region
     */
    public List<Region> findAll(){
        return regionRepository.findAll();
    }

    /**
     * find by ID
     */
    public Optional<Region> findById(Long regionId){
        return regionRepository.findById(regionId);
    }

    /**
     * deleting
     */
    public void deleteById(Long regionId){
        regionRepository.deleteById(regionId);
    }



}

package dejamobile.project.TravelPhoneBook.service;

import dejamobile.project.TravelPhoneBook.mappers.DepartmentMapper;
import dejamobile.project.TravelPhoneBook.mappers.RegionMapper;
import dejamobile.project.TravelPhoneBook.mappers.VileMapper;
import dejamobile.project.TravelPhoneBook.model.DTO.DepartmentDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Department;
import dejamobile.project.TravelPhoneBook.model.domain.Vile;
import dejamobile.project.TravelPhoneBook.repository.DepartmentRepository;
import dejamobile.project.TravelPhoneBook.repository.RegionRepository;
import dejamobile.project.TravelPhoneBook.repository.VileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class DService {

    private final DepartmentRepository departmentRepository;
    private final VileRepository vileRepository;
    private final DepartmentMapper departmentMapper;
    private final VileMapper vileMapper;
    private final RegionMapper regionMapper;
    private final RegionRepository regionRepository;


    @Autowired

    public DService(DepartmentRepository departmentRepository, VileRepository vileRepository,
                    DepartmentMapper departmentMapper, VileMapper vileMapper,
                    RegionMapper regionMapper, RegionRepository regionRepository) {
        this.departmentRepository = departmentRepository;
        this.vileRepository = vileRepository;
        this.departmentMapper = departmentMapper;
        this.vileMapper = vileMapper;
        this.regionMapper = regionMapper;
        this.regionRepository = regionRepository;
    }

    /**
     * to create a new department
     */
    public DepartmentDTO save(DepartmentDTO departmentDTO){
        Department department = departmentMapper.toDomain(departmentDTO);

        Department createdDepartment = departmentRepository.save(department);
        List<Vile> viles = new ArrayList<>();

        departmentDTO.getVileDTOs().forEach( vileDTO -> {
            Vile vile = vileMapper.toDomain(vileDTO);
            vile.setDepartment(createdDepartment);
            viles.add(vile);
        });

        departmentDTO.getDepRegion();

        createdDepartment.setViles(viles);
        vileRepository.saveAll(viles);

        return departmentMapper.toDto(createdDepartment);
    }


    /**
     * to get list of department
     */
    public List<Department> findAll(){
        return departmentRepository.findAll();
    }

    /**
     * find by ID
     */
    public Optional<Department> findById(Long departmentId){
        return departmentRepository.findById(departmentId);
    }

    /**
     * deleting
     */
    public void deleteById(Long departmentId){
        departmentRepository.deleteById(departmentId);
    }


}

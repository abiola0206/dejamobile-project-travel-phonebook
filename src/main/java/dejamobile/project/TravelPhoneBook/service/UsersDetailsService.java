package dejamobile.project.TravelPhoneBook.service;

import dejamobile.project.TravelPhoneBook.model.domain.Users;
import dejamobile.project.TravelPhoneBook.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;

@Service
public class UsersDetailsService implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;



    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        Users users  = usersRepository.findByUsername(username);
        if (users == null){
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return UserPrinciple.build(users);
    }

}

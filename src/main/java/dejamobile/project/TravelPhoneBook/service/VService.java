package dejamobile.project.TravelPhoneBook.service;



import dejamobile.project.TravelPhoneBook.mappers.VileMapper;
import dejamobile.project.TravelPhoneBook.model.DTO.VileDTO;
import dejamobile.project.TravelPhoneBook.model.domain.Vile;
import dejamobile.project.TravelPhoneBook.repository.VileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class VService {

    private final VileRepository vileRepository;

    private final VileMapper vileMapper;

    @Autowired
    public VService(VileRepository vileRepository, VileMapper vileMapper) {
        this.vileRepository = vileRepository;
        this.vileMapper = vileMapper;
    }

    /**
     * to create a new ville
     */
    public VileDTO save(VileDTO villeDTO){
        Vile vile = vileMapper.toDomain(villeDTO);

        Vile createdVile = vileRepository.save(vile);

        createdVile.setDepartment(vile.getDepartment());
        createdVile.setRegion(vile.getRegion());
        createdVile.setSports(vile.getSports());


        return vileMapper.toDto(createdVile);
    }


    /**
     * to get list of department
     */
    public List<Vile> findAll(){
        return vileRepository.findAll();
    }

    /**
     * find by ID
     */
    public Optional<Vile> findById(Long villeId){
        return vileRepository.findById(villeId);
    }

    /**
     * deleting
     */
    public void deleteById(Long vileId){
        vileRepository.deleteById(vileId);
    }

}

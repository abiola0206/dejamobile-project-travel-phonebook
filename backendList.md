Folder PATH listing for volume Windows
Volume serial number is 08D9-D14E
C:.
+---.idea
|   +---dataSources
|   |   \---6c84d49e-36b8-4caf-8b5a-36e799bc8828
|   |       \---storage_v2
|   |           \---_src_
|   |               \---schema
|   +---inspectionProfiles
|   \---libraries
+---.mvn
|   \---wrapper
+---src
|   +---main
|   |   +---java
|   |   |   \---dejamobile
|   |   |       \---project
|   |   |           \---TravelPhoneBook
|   |   |               +---api
|   |   |               +---config
|   |   |               |   \---userConfig
|   |   |               +---exception
|   |   |               +---mappers
|   |   |               +---model
|   |   |               |   +---domain
|   |   |               |   +---DTO
|   |   |               |   \---jwtModel
|   |   |               +---repository
|   |   |               \---service
|   |   \---resources
|   |       +---static
|   |       \---templates
|   \---test
|       \---java
|           \---dejamobile
|               \---project
|                   \---TravelPhoneBook
+---target
|   +---classes
|   |   \---dejamobile
|   |       \---project
|   |           \---TravelPhoneBook
|   |               +---api
|   |               +---config
|   |               |   \---userConfig
|   |               +---exception
|   |               +---mappers
|   |               +---model
|   |               |   +---domain
|   |               |   +---DTO
|   |               |   \---jwtModel
|   |               +---repository
|   |               \---service
|   +---generated-sources
|   |   \---annotations
|   |       \---dejamobile
|   |           \---project
|   |               \---TravelPhoneBook
|   |                   \---mappers
|   +---generated-test-sources
|   |   \---test-annotations
|   \---test-classes
|       \---dejamobile
|           \---project
|               \---TravelPhoneBook
\---TravelPhoneBookFront
    +---e2e
    |   \---src
    +---node_modules
    |   +---.bin
    |   +---@angular
    |   |   +---animations
    |   |   |   +---browser
    |   |   |   |   +---src
    |   |   |   |   |   +---dsl
    |   |   |   |   |   |   \---style_normalization
    |   |   |   |   |   \---render
    |   |   |   |   |       +---css_keyframes
    |   |   |   |   |       \---web_animations
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   +---browser
    |   |   |   |   |   +---src
    |   |   |   |   |   |   +---dsl
    |   |   |   |   |   |   |   \---style_normalization
    |   |   |   |   |   |   \---render
    |   |   |   |   |   |       +---css_keyframes
    |   |   |   |   |   |       \---web_animations
    |   |   |   |   |   \---testing
    |   |   |   |   |       \---src
    |   |   |   |   \---src
    |   |   |   |       \---players
    |   |   |   +---esm5
    |   |   |   |   +---browser
    |   |   |   |   |   +---src
    |   |   |   |   |   |   +---dsl
    |   |   |   |   |   |   |   \---style_normalization
    |   |   |   |   |   |   \---render
    |   |   |   |   |   |       +---css_keyframes
    |   |   |   |   |   |       \---web_animations
    |   |   |   |   |   \---testing
    |   |   |   |   |       \---src
    |   |   |   |   \---src
    |   |   |   |       \---players
    |   |   |   +---fesm2015
    |   |   |   |   \---browser
    |   |   |   +---fesm5
    |   |   |   |   \---browser
    |   |   |   \---src
    |   |   |       \---players
    |   |   +---cli
    |   |   |   +---bin
    |   |   |   |   \---postinstall
    |   |   |   +---commands
    |   |   |   +---lib
    |   |   |   |   +---cli
    |   |   |   |   \---config
    |   |   |   +---models
    |   |   |   +---node_modules
    |   |   |   |   +---.bin
    |   |   |   |   +---debug
    |   |   |   |   |   \---src
    |   |   |   |   +---lru-cache
    |   |   |   |   +---ms
    |   |   |   |   +---rimraf
    |   |   |   |   +---semver
    |   |   |   |   |   +---bin
    |   |   |   |   |   +---classes
    |   |   |   |   |   +---functions
    |   |   |   |   |   +---internal
    |   |   |   |   |   \---ranges
    |   |   |   |   +---uuid
    |   |   |   |   |   \---dist
    |   |   |   |   |       +---bin
    |   |   |   |   |       +---esm-browser
    |   |   |   |   |       +---esm-node
    |   |   |   |   |       \---umd
    |   |   |   |   \---yallist
    |   |   |   \---utilities
    |   |   +---common
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   +---http
    |   |   |   |   |   +---src
    |   |   |   |   |   \---testing
    |   |   |   |   |       \---src
    |   |   |   |   +---src
    |   |   |   |   |   +---directives
    |   |   |   |   |   +---i18n
    |   |   |   |   |   +---location
    |   |   |   |   |   \---pipes
    |   |   |   |   |       \---deprecated
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---esm5
    |   |   |   |   +---http
    |   |   |   |   |   +---src
    |   |   |   |   |   \---testing
    |   |   |   |   |       \---src
    |   |   |   |   +---src
    |   |   |   |   |   +---directives
    |   |   |   |   |   +---i18n
    |   |   |   |   |   +---location
    |   |   |   |   |   \---pipes
    |   |   |   |   |       \---deprecated
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---fesm2015
    |   |   |   |   \---http
    |   |   |   +---fesm5
    |   |   |   |   \---http
    |   |   |   +---http
    |   |   |   |   +---src
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---locales
    |   |   |   |   \---extra
    |   |   |   +---src
    |   |   |   |   +---directives
    |   |   |   |   +---i18n
    |   |   |   |   +---location
    |   |   |   |   \---pipes
    |   |   |   |       \---deprecated
    |   |   |   \---testing
    |   |   |       \---src
    |   |   +---compiler
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   +---src
    |   |   |   |   |   +---aot
    |   |   |   |   |   +---compiler_util
    |   |   |   |   |   +---css_parser
    |   |   |   |   |   +---expression_parser
    |   |   |   |   |   +---i18n
    |   |   |   |   |   |   \---serializers
    |   |   |   |   |   +---jit
    |   |   |   |   |   +---ml_parser
    |   |   |   |   |   +---output
    |   |   |   |   |   +---render3
    |   |   |   |   |   |   \---view
    |   |   |   |   |   |       \---i18n
    |   |   |   |   |   +---schema
    |   |   |   |   |   +---template_parser
    |   |   |   |   |   \---view_compiler
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   |           \---output
    |   |   |   +---esm5
    |   |   |   |   +---src
    |   |   |   |   |   +---aot
    |   |   |   |   |   +---compiler_util
    |   |   |   |   |   +---css_parser
    |   |   |   |   |   +---expression_parser
    |   |   |   |   |   +---i18n
    |   |   |   |   |   |   \---serializers
    |   |   |   |   |   +---jit
    |   |   |   |   |   +---ml_parser
    |   |   |   |   |   +---output
    |   |   |   |   |   +---render3
    |   |   |   |   |   |   \---view
    |   |   |   |   |   |       \---i18n
    |   |   |   |   |   +---schema
    |   |   |   |   |   +---template_parser
    |   |   |   |   |   \---view_compiler
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   |           \---output
    |   |   |   +---fesm2015
    |   |   |   +---fesm5
    |   |   |   +---src
    |   |   |   |   +---aot
    |   |   |   |   +---compiler_util
    |   |   |   |   +---css_parser
    |   |   |   |   +---expression_parser
    |   |   |   |   +---i18n
    |   |   |   |   |   \---serializers
    |   |   |   |   +---jit
    |   |   |   |   +---ml_parser
    |   |   |   |   +---output
    |   |   |   |   +---render3
    |   |   |   |   |   \---view
    |   |   |   |   |       \---i18n
    |   |   |   |   +---schema
    |   |   |   |   +---template_parser
    |   |   |   |   \---view_compiler
    |   |   |   \---testing
    |   |   |       \---src
    |   |   |           \---output
    |   |   +---compiler-cli
    |   |   |   +---node_modules
    |   |   |   |   +---ansi-regex
    |   |   |   |   +---camelcase
    |   |   |   |   +---chokidar
    |   |   |   |   |   +---lib
    |   |   |   |   |   \---types
    |   |   |   |   +---cliui
    |   |   |   |   |   \---node_modules
    |   |   |   |   |       \---string-width
    |   |   |   |   +---cross-spawn
    |   |   |   |   |   \---lib
    |   |   |   |   |       \---util
    |   |   |   |   +---execa
    |   |   |   |   |   \---lib
    |   |   |   |   +---get-caller-file
    |   |   |   |   +---get-stream
    |   |   |   |   +---invert-kv
    |   |   |   |   +---is-fullwidth-code-point
    |   |   |   |   +---lcid
    |   |   |   |   +---normalize-path
    |   |   |   |   +---os-locale
    |   |   |   |   +---require-main-filename
    |   |   |   |   +---source-map
    |   |   |   |   |   +---dist
    |   |   |   |   |   \---lib
    |   |   |   |   +---string-width
    |   |   |   |   |   \---node_modules
    |   |   |   |   |       +---ansi-regex
    |   |   |   |   |       \---strip-ansi
    |   |   |   |   +---strip-ansi
    |   |   |   |   +---which-module
    |   |   |   |   +---yargs
    |   |   |   |   |   +---lib
    |   |   |   |   |   \---locales
    |   |   |   |   \---yargs-parser
    |   |   |   |       \---lib
    |   |   |   \---src
    |   |   |       +---diagnostics
    |   |   |       +---metadata
    |   |   |       +---ngcc
    |   |   |       |   \---src
    |   |   |       |       +---analysis
    |   |   |       |       +---host
    |   |   |       |       +---packages
    |   |   |       |       \---rendering
    |   |   |       +---ngtsc
    |   |   |       |   +---annotations
    |   |   |       |   |   \---src
    |   |   |       |   +---diagnostics
    |   |   |       |   |   \---src
    |   |   |       |   +---host
    |   |   |       |   |   \---src
    |   |   |       |   +---metadata
    |   |   |       |   |   \---src
    |   |   |       |   +---shims
    |   |   |       |   |   \---src
    |   |   |       |   +---switch
    |   |   |       |   |   \---src
    |   |   |       |   +---transform
    |   |   |       |   |   \---src
    |   |   |       |   +---translator
    |   |   |       |   |   \---src
    |   |   |       |   +---typecheck
    |   |   |       |   |   \---src
    |   |   |       |   \---util
    |   |   |       |       \---src
    |   |   |       \---transformers
    |   |   +---core
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   +---src
    |   |   |   |   |   +---change_detection
    |   |   |   |   |   |   \---differs
    |   |   |   |   |   +---debug
    |   |   |   |   |   +---di
    |   |   |   |   |   +---i18n
    |   |   |   |   |   +---linker
    |   |   |   |   |   +---metadata
    |   |   |   |   |   +---profile
    |   |   |   |   |   +---reflection
    |   |   |   |   |   +---render
    |   |   |   |   |   +---render3
    |   |   |   |   |   |   +---features
    |   |   |   |   |   |   +---interfaces
    |   |   |   |   |   |   +---jit
    |   |   |   |   |   |   \---styling
    |   |   |   |   |   +---sanitization
    |   |   |   |   |   +---testability
    |   |   |   |   |   +---util
    |   |   |   |   |   +---view
    |   |   |   |   |   \---zone
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---esm5
    |   |   |   |   +---src
    |   |   |   |   |   +---change_detection
    |   |   |   |   |   |   \---differs
    |   |   |   |   |   +---debug
    |   |   |   |   |   +---di
    |   |   |   |   |   +---i18n
    |   |   |   |   |   +---linker
    |   |   |   |   |   +---metadata
    |   |   |   |   |   +---profile
    |   |   |   |   |   +---reflection
    |   |   |   |   |   +---render
    |   |   |   |   |   +---render3
    |   |   |   |   |   |   +---features
    |   |   |   |   |   |   +---interfaces
    |   |   |   |   |   |   +---jit
    |   |   |   |   |   |   \---styling
    |   |   |   |   |   +---sanitization
    |   |   |   |   |   +---testability
    |   |   |   |   |   +---util
    |   |   |   |   |   +---view
    |   |   |   |   |   \---zone
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---fesm2015
    |   |   |   +---fesm5
    |   |   |   +---src
    |   |   |   |   +---change_detection
    |   |   |   |   |   \---differs
    |   |   |   |   +---debug
    |   |   |   |   +---di
    |   |   |   |   +---i18n
    |   |   |   |   +---linker
    |   |   |   |   +---metadata
    |   |   |   |   +---profile
    |   |   |   |   +---reflection
    |   |   |   |   +---render
    |   |   |   |   +---render3
    |   |   |   |   |   +---features
    |   |   |   |   |   +---interfaces
    |   |   |   |   |   +---jit
    |   |   |   |   |   \---styling
    |   |   |   |   +---sanitization
    |   |   |   |   +---testability
    |   |   |   |   +---util
    |   |   |   |   +---view
    |   |   |   |   \---zone
    |   |   |   \---testing
    |   |   |       \---src
    |   |   +---forms
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   \---src
    |   |   |   |       \---directives
    |   |   |   |           \---reactive_directives
    |   |   |   +---esm5
    |   |   |   |   \---src
    |   |   |   |       \---directives
    |   |   |   |           \---reactive_directives
    |   |   |   +---fesm2015
    |   |   |   +---fesm5
    |   |   |   \---src
    |   |   |       \---directives
    |   |   |           \---reactive_directives
    |   |   +---language-service
    |   |   |   +---bundles
    |   |   |   \---src
    |   |   +---platform-browser
    |   |   |   +---animations
    |   |   |   |   \---src
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   +---animations
    |   |   |   |   |   \---src
    |   |   |   |   +---src
    |   |   |   |   |   +---browser
    |   |   |   |   |   |   +---location
    |   |   |   |   |   |   \---tools
    |   |   |   |   |   +---dom
    |   |   |   |   |   |   +---debug
    |   |   |   |   |   |   \---events
    |   |   |   |   |   \---security
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---esm5
    |   |   |   |   +---animations
    |   |   |   |   |   \---src
    |   |   |   |   +---src
    |   |   |   |   |   +---browser
    |   |   |   |   |   |   +---location
    |   |   |   |   |   |   \---tools
    |   |   |   |   |   +---dom
    |   |   |   |   |   |   +---debug
    |   |   |   |   |   |   \---events
    |   |   |   |   |   \---security
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---fesm2015
    |   |   |   +---fesm5
    |   |   |   +---src
    |   |   |   |   +---browser
    |   |   |   |   |   +---location
    |   |   |   |   |   \---tools
    |   |   |   |   +---dom
    |   |   |   |   |   +---debug
    |   |   |   |   |   \---events
    |   |   |   |   \---security
    |   |   |   \---testing
    |   |   |       \---src
    |   |   +---platform-browser-dynamic
    |   |   |   +---bundles
    |   |   |   +---esm2015
    |   |   |   |   +---src
    |   |   |   |   |   \---resource_loader
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---esm5
    |   |   |   |   +---src
    |   |   |   |   |   \---resource_loader
    |   |   |   |   \---testing
    |   |   |   |       \---src
    |   |   |   +---fesm2015
    |   |   |   +---fesm5
    |   |   |   +---src
    |   |   |   |   \---resource_loader
    |   |   |   \---testing
    |   |   |       \---src
    |   |   \---router
    |   |       +---bundles
    |   |       +---esm2015
    |   |       |   +---src
    |   |       |   |   +---components
    |   |       |   |   +---directives
    |   |       |   |   +---operators
    |   |       |   |   \---utils
    |   |       |   +---testing
    |   |       |   |   \---src
    |   |       |   \---upgrade
    |   |       |       \---src
    |   |       +---esm5
    |   |       |   +---src
    |   |       |   |   +---components
    |   |       |   |   +---directives
    |   |       |   |   +---operators
    |   |       |   |   \---utils
    |   |       |   +---testing
    |   |       |   |   \---src
    |   |       |   \---upgrade
    |   |       |       \---src
    |   |       +---fesm2015
    |   |       +---fesm5
    |   |       +---src
    |   |       |   +---components
    |   |       |   +---directives
    |   |       |   +---operators
    |   |       |   \---utils
    |   |       +---testing
    |   |       |   \---src
    |   |       \---upgrade
    |   |           \---src
    |   +---@angular-devkit
    |   |   +---architect
    |   |   |   +---builders
    |   |   |   +---node
    |   |   |   +---node_modules
    |   |   |   |   \---rxjs
    |   |   |   |       +---add
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   \---operator
    |   |   |   |       +---ajax
    |   |   |   |       +---bundles
    |   |   |   |       +---fetch
    |   |   |   |       +---internal
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---scheduled
    |   |   |   |       |   +---scheduler
    |   |   |   |       |   +---symbol
    |   |   |   |       |   +---testing
    |   |   |   |       |   \---util
    |   |   |   |       +---internal-compatibility
    |   |   |   |       +---migrations
    |   |   |   |       |   \---update-6_0_0
    |   |   |   |       +---observable
    |   |   |   |       |   \---dom
    |   |   |   |       +---operator
    |   |   |   |       +---operators
    |   |   |   |       +---scheduler
    |   |   |   |       +---src
    |   |   |   |       |   +---add
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   \---operator
    |   |   |   |       |   +---ajax
    |   |   |   |       |   +---fetch
    |   |   |   |       |   +---internal
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   +---operators
    |   |   |   |       |   |   +---scheduled
    |   |   |   |       |   |   +---scheduler
    |   |   |   |       |   |   +---symbol
    |   |   |   |       |   |   +---testing
    |   |   |   |       |   |   \---util
    |   |   |   |       |   +---internal-compatibility
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   +---operator
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---scheduler
    |   |   |   |       |   +---symbol
    |   |   |   |       |   +---testing
    |   |   |   |       |   +---util
    |   |   |   |       |   \---webSocket
    |   |   |   |       +---symbol
    |   |   |   |       +---testing
    |   |   |   |       +---util
    |   |   |   |       +---webSocket
    |   |   |   |       +---_esm2015
    |   |   |   |       |   +---ajax
    |   |   |   |       |   +---fetch
    |   |   |   |       |   +---internal
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   +---operators
    |   |   |   |       |   |   +---scheduled
    |   |   |   |       |   |   +---scheduler
    |   |   |   |       |   |   +---symbol
    |   |   |   |       |   |   +---testing
    |   |   |   |       |   |   \---util
    |   |   |   |       |   +---internal-compatibility
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---testing
    |   |   |   |       |   \---webSocket
    |   |   |   |       \---_esm5
    |   |   |   |           +---ajax
    |   |   |   |           +---fetch
    |   |   |   |           +---internal
    |   |   |   |           |   +---observable
    |   |   |   |           |   |   \---dom
    |   |   |   |           |   +---operators
    |   |   |   |           |   +---scheduled
    |   |   |   |           |   +---scheduler
    |   |   |   |           |   +---symbol
    |   |   |   |           |   +---testing
    |   |   |   |           |   \---util
    |   |   |   |           +---internal-compatibility
    |   |   |   |           +---operators
    |   |   |   |           +---testing
    |   |   |   |           \---webSocket
    |   |   |   +---src
    |   |   |   \---testing
    |   |   +---build-angular
    |   |   |   +---node_modules
    |   |   |   |   +---.bin
    |   |   |   |   +---@angular-devkit
    |   |   |   |   |   +---architect
    |   |   |   |   |   |   +---builders
    |   |   |   |   |   |   +---node
    |   |   |   |   |   |   +---src
    |   |   |   |   |   |   \---testing
    |   |   |   |   |   \---core
    |   |   |   |   |       +---node
    |   |   |   |   |       |   +---experimental
    |   |   |   |   |       |   |   \---jobs
    |   |   |   |   |       |   \---testing
    |   |   |   |   |       \---src
    |   |   |   |   |           +---analytics
    |   |   |   |   |           +---exception
    |   |   |   |   |           +---experimental
    |   |   |   |   |           |   \---jobs
    |   |   |   |   |           +---json
    |   |   |   |   |           |   \---schema
    |   |   |   |   |           +---logger
    |   |   |   |   |           +---utils
    |   |   |   |   |           +---virtual-fs
    |   |   |   |   |           |   \---host
    |   |   |   |   |           \---workspace
    |   |   |   |   |               \---json
    |   |   |   |   |                   \---test
    |   |   |   |   |                       \---cases
    |   |   |   |   +---ajv
    |   |   |   |   |   +---dist
    |   |   |   |   |   +---lib
    |   |   |   |   |   |   +---compile
    |   |   |   |   |   |   +---dot
    |   |   |   |   |   |   +---dotjs
    |   |   |   |   |   |   \---refs
    |   |   |   |   |   \---scripts
    |   |   |   |   +---ansi-escapes
    |   |   |   |   +---ansi-regex
    |   |   |   |   +---ansi-styles
    |   |   |   |   +---chalk
    |   |   |   |   |   \---source
    |   |   |   |   +---cli-cursor
    |   |   |   |   +---cli-width
    |   |   |   |   |   \---.nyc_output
    |   |   |   |   |       \---processinfo
    |   |   |   |   +---color-convert
    |   |   |   |   +---color-name
    |   |   |   |   +---core-js
    |   |   |   |   |   +---es
    |   |   |   |   |   |   +---array
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---array-buffer
    |   |   |   |   |   |   +---data-view
    |   |   |   |   |   |   +---date
    |   |   |   |   |   |   +---function
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---instance
    |   |   |   |   |   |   +---json
    |   |   |   |   |   |   +---map
    |   |   |   |   |   |   +---math
    |   |   |   |   |   |   +---number
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---object
    |   |   |   |   |   |   +---promise
    |   |   |   |   |   |   +---reflect
    |   |   |   |   |   |   +---regexp
    |   |   |   |   |   |   +---set
    |   |   |   |   |   |   +---string
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   +---typed-array
    |   |   |   |   |   |   +---weak-map
    |   |   |   |   |   |   \---weak-set
    |   |   |   |   |   +---features
    |   |   |   |   |   |   +---array
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---array-buffer
    |   |   |   |   |   |   +---async-iterator
    |   |   |   |   |   |   +---bigint
    |   |   |   |   |   |   +---data-view
    |   |   |   |   |   |   +---date
    |   |   |   |   |   |   +---dom-collections
    |   |   |   |   |   |   +---function
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---instance
    |   |   |   |   |   |   +---iterator
    |   |   |   |   |   |   +---json
    |   |   |   |   |   |   +---map
    |   |   |   |   |   |   +---math
    |   |   |   |   |   |   +---number
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---object
    |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   +---promise
    |   |   |   |   |   |   +---reflect
    |   |   |   |   |   |   +---regexp
    |   |   |   |   |   |   +---set
    |   |   |   |   |   |   +---string
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   +---typed-array
    |   |   |   |   |   |   +---url
    |   |   |   |   |   |   +---url-search-params
    |   |   |   |   |   |   +---weak-map
    |   |   |   |   |   |   \---weak-set
    |   |   |   |   |   +---internals
    |   |   |   |   |   +---modules
    |   |   |   |   |   +---proposals
    |   |   |   |   |   +---stable
    |   |   |   |   |   |   +---array
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---array-buffer
    |   |   |   |   |   |   +---data-view
    |   |   |   |   |   |   +---date
    |   |   |   |   |   |   +---dom-collections
    |   |   |   |   |   |   +---function
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---instance
    |   |   |   |   |   |   +---json
    |   |   |   |   |   |   +---map
    |   |   |   |   |   |   +---math
    |   |   |   |   |   |   +---number
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---object
    |   |   |   |   |   |   +---promise
    |   |   |   |   |   |   +---reflect
    |   |   |   |   |   |   +---regexp
    |   |   |   |   |   |   +---set
    |   |   |   |   |   |   +---string
    |   |   |   |   |   |   |   \---virtual
    |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   +---typed-array
    |   |   |   |   |   |   +---url
    |   |   |   |   |   |   +---url-search-params
    |   |   |   |   |   |   +---weak-map
    |   |   |   |   |   |   \---weak-set
    |   |   |   |   |   +---stage
    |   |   |   |   |   \---web
    |   |   |   |   +---emoji-regex
    |   |   |   |   |   \---es2015
    |   |   |   |   +---fast-deep-equal
    |   |   |   |   |   \---es6
    |   |   |   |   +---fast-json-stable-stringify
    |   |   |   |   |   +---.github
    |   |   |   |   |   +---benchmark
    |   |   |   |   |   +---example
    |   |   |   |   |   \---test
    |   |   |   |   +---figures
    |   |   |   |   +---glob
    |   |   |   |   +---has-flag
    |   |   |   |   +---inquirer
    |   |   |   |   |   \---lib
    |   |   |   |   |       +---objects
    |   |   |   |   |       +---prompts
    |   |   |   |   |       +---ui
    |   |   |   |   |       \---utils
    |   |   |   |   +---is-fullwidth-code-point
    |   |   |   |   +---is-wsl
    |   |   |   |   +---lru-cache
    |   |   |   |   +---mute-stream
    |   |   |   |   +---onetime
    |   |   |   |   +---open
    |   |   |   |   +---restore-cursor
    |   |   |   |   +---rimraf
    |   |   |   |   +---rxjs
    |   |   |   |   |   +---add
    |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   |   \---dom
    |   |   |   |   |   |   \---operator
    |   |   |   |   |   +---ajax
    |   |   |   |   |   +---bundles
    |   |   |   |   |   +---fetch
    |   |   |   |   |   +---internal
    |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   |   \---dom
    |   |   |   |   |   |   +---operators
    |   |   |   |   |   |   +---scheduled
    |   |   |   |   |   |   +---scheduler
    |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   +---testing
    |   |   |   |   |   |   \---util
    |   |   |   |   |   +---internal-compatibility
    |   |   |   |   |   +---migrations
    |   |   |   |   |   |   \---update-6_0_0
    |   |   |   |   |   +---observable
    |   |   |   |   |   |   \---dom
    |   |   |   |   |   +---operator
    |   |   |   |   |   +---operators
    |   |   |   |   |   +---scheduler
    |   |   |   |   |   +---src
    |   |   |   |   |   |   +---add
    |   |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   |   |   \---dom
    |   |   |   |   |   |   |   \---operator
    |   |   |   |   |   |   +---ajax
    |   |   |   |   |   |   +---fetch
    |   |   |   |   |   |   +---internal
    |   |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   |   |   \---dom
    |   |   |   |   |   |   |   +---operators
    |   |   |   |   |   |   |   +---scheduled
    |   |   |   |   |   |   |   +---scheduler
    |   |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   |   +---testing
    |   |   |   |   |   |   |   \---util
    |   |   |   |   |   |   +---internal-compatibility
    |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   |   \---dom
    |   |   |   |   |   |   +---operator
    |   |   |   |   |   |   +---operators
    |   |   |   |   |   |   +---scheduler
    |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   +---testing
    |   |   |   |   |   |   +---util
    |   |   |   |   |   |   \---webSocket
    |   |   |   |   |   +---symbol
    |   |   |   |   |   +---testing
    |   |   |   |   |   +---util
    |   |   |   |   |   +---webSocket
    |   |   |   |   |   +---_esm2015
    |   |   |   |   |   |   +---ajax
    |   |   |   |   |   |   +---fetch
    |   |   |   |   |   |   +---internal
    |   |   |   |   |   |   |   +---observable
    |   |   |   |   |   |   |   |   \---dom
    |   |   |   |   |   |   |   +---operators
    |   |   |   |   |   |   |   +---scheduled
    |   |   |   |   |   |   |   +---scheduler
    |   |   |   |   |   |   |   +---symbol
    |   |   |   |   |   |   |   +---testing
    |   |   |   |   |   |   |   \---util
    |   |   |   |   |   |   +---internal-compatibility
    |   |   |   |   |   |   +---operators
    |   |   |   |   |   |   +---testing
    |   |   |   |   |   |   \---webSocket
    |   |   |   |   |   \---_esm5
    |   |   |   |   |       +---ajax
    |   |   |   |   |       +---fetch
    |   |   |   |   |       +---internal
    |   |   |   |   |       |   +---observable
    |   |   |   |   |       |   |   \---dom
    |   |   |   |   |       |   +---operators
    |   |   |   |   |       |   +---scheduled
    |   |   |   |   |       |   +---scheduler
    |   |   |   |   |       |   +---symbol
    |   |   |   |   |       |   +---testing
    |   |   |   |   |       |   \---util
    |   |   |   |   |       +---internal-compatibility
    |   |   |   |   |       +---operators
    |   |   |   |   |       +---testing
    |   |   |   |   |       \---webSocket
    |   |   |   |   +---semver
    |   |   |   |   |   +---bin
    |   |   |   |   |   +---classes
    |   |   |   |   |   +---functions
    |   |   |   |   |   +---internal
    |   |   |   |   |   \---ranges
    |   |   |   |   +---source-map-support
    |   |   |   |   |   \---node_modules
    |   |   |   |   |       \---source-map
    |   |   |   |   |           +---dist
    |   |   |   |   |           \---lib
    |   |   |   |   +---string-width
    |   |   |   |   +---strip-ansi
    |   |   |   |   +---supports-color
    |   |   |   |   \---yallist
    |   |   |   +---plugins
    |   |   |   +---src
    |   |   |   |   +---app-shell
    |   |   |   |   +---babel
    |   |   |   |   |   \---presets
    |   |   |   |   +---browser
    |   |   |   |   |   \---tests
    |   |   |   |   +---dev-server
    |   |   |   |   +---extract-i18n
    |   |   |   |   +---karma
    |   |   |   |   +---ng-packagr
    |   |   |   |   +---protractor
    |   |   |   |   +---server
    |   |   |   |   +---testing
    |   |   |   |   +---tslint
    |   |   |   |   +---utils
    |   |   |   |   |   \---index-file
    |   |   |   |   \---webpack
    |   |   |   |       +---configs
    |   |   |   |       +---plugins
    |   |   |   |       |   \---hmr
    |   |   |   |       \---utils
    |   |   |   \---test
    |   |   |       \---hello-world-lib
    |   |   |           \---projects
    |   |   |               \---lib
    |   |   +---build-optimizer
    |   |   |   +---node_modules
    |   |   |   |   +---.bin
    |   |   |   |   +---tslib
    |   |   |   |   |   \---modules
    |   |   |   |   \---typescript
    |   |   |   |       +---bin
    |   |   |   |       +---lib
    |   |   |   |       |   +---cs
    |   |   |   |       |   +---de
    |   |   |   |       |   +---es
    |   |   |   |       |   +---fr
    |   |   |   |       |   +---it
    |   |   |   |       |   +---ja
    |   |   |   |       |   +---ko
    |   |   |   |       |   +---pl
    |   |   |   |       |   +---pt-br
    |   |   |   |       |   +---ru
    |   |   |   |       |   +---tr
    |   |   |   |       |   +---zh-cn
    |   |   |   |       |   \---zh-tw
    |   |   |   |       \---loc
    |   |   |   |           \---lcl
    |   |   |   |               +---CHS
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---CHT
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---CSY
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---DEU
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---ESN
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---FRA
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---ITA
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---JPN
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---KOR
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---PLK
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---PTB
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               +---RUS
    |   |   |   |               |   +---Targets
    |   |   |   |               |   +---TypeScriptDebugEngine
    |   |   |   |               |   +---TypeScriptLanguageService
    |   |   |   |               |   \---TypeScriptTasks
    |   |   |   |               \---TRK
    |   |   |   |                   +---Targets
    |   |   |   |                   +---TypeScriptDebugEngine
    |   |   |   |                   +---TypeScriptLanguageService
    |   |   |   |                   \---TypeScriptTasks
    |   |   |   +---src
    |   |   |   |   +---build-optimizer
    |   |   |   |   +---helpers
    |   |   |   |   \---transforms
    |   |   |   \---webpack-loader
    |   |   +---build-webpack
    |   |   |   +---node_modules
    |   |   |   |   +---@angular-devkit
    |   |   |   |   |   +---architect
    |   |   |   |   |   |   +---builders
    |   |   |   |   |   |   +---node
    |   |   |   |   |   |   +---src
    |   |   |   |   |   |   \---testing
    |   |   |   |   |   \---core
    |   |   |   |   |       +---node
    |   |   |   |   |       |   +---experimental
    |   |   |   |   |       |   |   \---jobs
    |   |   |   |   |       |   \---testing
    |   |   |   |   |       \---src
    |   |   |   |   |           +---analytics
    |   |   |   |   |           +---exception
    |   |   |   |   |           +---experimental
    |   |   |   |   |           |   \---jobs
    |   |   |   |   |           +---json
    |   |   |   |   |           |   \---schema
    |   |   |   |   |           +---logger
    |   |   |   |   |           +---utils
    |   |   |   |   |           +---virtual-fs
    |   |   |   |   |           |   \---host
    |   |   |   |   |           \---workspace
    |   |   |   |   |               \---json
    |   |   |   |   |                   \---test
    |   |   |   |   |                       \---cases
    |   |   |   |   +---ajv
    |   |   |   |   |   +---dist
    |   |   |   |   |   +---lib
    |   |   |   |   |   |   +---compile
    |   |   |   |   |   |   +---dot
    |   |   |   |   |   |   +---dotjs
    |   |   |   |   |   |   \---refs
    |   |   |   |   |   \---scripts
    |   |   |   |   +---fast-deep-equal
    |   |   |   |   |   \---es6
    |   |   |   |   +---fast-json-stable-stringify
    |   |   |   |   |   +---.github
    |   |   |   |   |   +---benchmark
    |   |   |   |   |   +---example
    |   |   |   |   |   \---test
    |   |   |   |   \---rxjs
    |   |   |   |       +---add
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   \---operator
    |   |   |   |       +---ajax
    |   |   |   |       +---bundles
    |   |   |   |       +---fetch
    |   |   |   |       +---internal
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---scheduled
    |   |   |   |       |   +---scheduler
    |   |   |   |       |   +---symbol
    |   |   |   |       |   +---testing
    |   |   |   |       |   \---util
    |   |   |   |       +---internal-compatibility
    |   |   |   |       +---migrations
    |   |   |   |       |   \---update-6_0_0
    |   |   |   |       +---observable
    |   |   |   |       |   \---dom
    |   |   |   |       +---operator
    |   |   |   |       +---operators
    |   |   |   |       +---scheduler
    |   |   |   |       +---src
    |   |   |   |       |   +---add
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   \---operator
    |   |   |   |       |   +---ajax
    |   |   |   |       |   +---fetch
    |   |   |   |       |   +---internal
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   +---operators
    |   |   |   |       |   |   +---scheduled
    |   |   |   |       |   |   +---scheduler
    |   |   |   |       |   |   +---symbol
    |   |   |   |       |   |   +---testing
    |   |   |   |       |   |   \---util
    |   |   |   |       |   +---internal-compatibility
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   +---operator
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---scheduler
    |   |   |   |       |   +---symbol
    |   |   |   |       |   +---testing
    |   |   |   |       |   +---util
    |   |   |   |       |   \---webSocket
    |   |   |   |       +---symbol
    |   |   |   |       +---testing
    |   |   |   |       +---util
    |   |   |   |       +---webSocket
    |   |   |   |       +---_esm2015
    |   |   |   |       |   +---ajax
    |   |   |   |       |   +---fetch
    |   |   |   |       |   +---internal
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   +---operators
    |   |   |   |       |   |   +---scheduled
    |   |   |   |       |   |   +---scheduler
    |   |   |   |       |   |   +---symbol
    |   |   |   |       |   |   +---testing
    |   |   |   |       |   |   \---util
    |   |   |   |       |   +---internal-compatibility
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---testing
    |   |   |   |       |   \---webSocket
    |   |   |   |       \---_esm5
    |   |   |   |           +---ajax
    |   |   |   |           +---fetch
    |   |   |   |           +---internal
    |   |   |   |           |   +---observable
    |   |   |   |           |   |   \---dom
    |   |   |   |           |   +---operators
    |   |   |   |           |   +---scheduled
    |   |   |   |           |   +---scheduler
    |   |   |   |           |   +---symbol
    |   |   |   |           |   +---testing
    |   |   |   |           |   \---util
    |   |   |   |           +---internal-compatibility
    |   |   |   |           +---operators
    |   |   |   |           +---testing
    |   |   |   |           \---webSocket
    |   |   |   \---src
    |   |   |       +---webpack
    |   |   |       \---webpack-dev-server
    |   |   +---core
    |   |   |   +---node
    |   |   |   |   +---experimental
    |   |   |   |   |   \---jobs
    |   |   |   |   \---testing
    |   |   |   +---node_modules
    |   |   |   |   +---ajv
    |   |   |   |   |   +---dist
    |   |   |   |   |   +---lib
    |   |   |   |   |   |   +---compile
    |   |   |   |   |   |   +---dot
    |   |   |   |   |   |   +---dotjs
    |   |   |   |   |   |   \---refs
    |   |   |   |   |   \---scripts
    |   |   |   |   +---fast-deep-equal
    |   |   |   |   |   \---es6
    |   |   |   |   +---fast-json-stable-stringify
    |   |   |   |   |   +---.github
    |   |   |   |   |   +---benchmark
    |   |   |   |   |   +---example
    |   |   |   |   |   \---test
    |   |   |   |   \---rxjs
    |   |   |   |       +---add
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   \---operator
    |   |   |   |       +---ajax
    |   |   |   |       +---bundles
    |   |   |   |       +---fetch
    |   |   |   |       +---internal
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---scheduled
    |   |   |   |       |   +---scheduler
    |   |   |   |       |   +---symbol
    |   |   |   |       |   +---testing
    |   |   |   |       |   \---util
    |   |   |   |       +---internal-compatibility
    |   |   |   |       +---migrations
    |   |   |   |       |   \---update-6_0_0
    |   |   |   |       +---observable
    |   |   |   |       |   \---dom
    |   |   |   |       +---operator
    |   |   |   |       +---operators
    |   |   |   |       +---scheduler
    |   |   |   |       +---src
    |   |   |   |       |   +---add
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   \---operator
    |   |   |   |       |   +---ajax
    |   |   |   |       |   +---fetch
    |   |   |   |       |   +---internal
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   +---operators
    |   |   |   |       |   |   +---scheduled
    |   |   |   |       |   |   +---scheduler
    |   |   |   |       |   |   +---symbol
    |   |   |   |       |   |   +---testing
    |   |   |   |       |   |   \---util
    |   |   |   |       |   +---internal-compatibility
    |   |   |   |       |   +---observable
    |   |   |   |       |   |   \---dom
    |   |   |   |       |   +---operator
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---scheduler
    |   |   |   |       |   +---symbol
    |   |   |   |       |   +---testing
    |   |   |   |       |   +---util
    |   |   |   |       |   \---webSocket
    |   |   |   |       +---symbol
    |   |   |   |       +---testing
    |   |   |   |       +---util
    |   |   |   |       +---webSocket
    |   |   |   |       +---_esm2015
    |   |   |   |       |   +---ajax
    |   |   |   |       |   +---fetch
    |   |   |   |       |   +---internal
    |   |   |   |       |   |   +---observable
    |   |   |   |       |   |   |   \---dom
    |   |   |   |       |   |   +---operators
    |   |   |   |       |   |   +---scheduled
    |   |   |   |       |   |   +---scheduler
    |   |   |   |       |   |   +---symbol
    |   |   |   |       |   |   +---testing
    |   |   |   |       |   |   \---util
    |   |   |   |       |   +---internal-compatibility
    |   |   |   |       |   +---operators
    |   |   |   |       |   +---testing
    |   |   |   |       |   \---webSocket
    |   |   |   |       \---_esm5
    |   |   |   |           +---ajax
    |   |   |   |           +---fetch
    |   |   |   |           +---internal
    |   |   |   |           |   +---observable
    |   |   |   |           |   |   \---dom
    |   |   |   |           |   +---operators
    |   |   |   |           |   +---scheduled
    |   |   |   |           |   +---scheduler
    |   |   |   |           |   +---symbol
    |   |   |   |           |   +---testing
    |   |   |   |           |   \---util
    |   |   |   |           +---internal-compatibility
    |   |   |   |           +---operators
    |   |   |   |           +---testing
    |   |   |   |           \---webSocket
    |   |   |   \---src
    |   |   |       +---analytics
    |   |   |       +---exception
    |   |   |       +---experimental
    |   |   |       |   \---jobs
    |   |   |       +---json
    |   |   |       |   \---schema
    |   |   |       +---logger
    |   |   |       +---utils
    |   |   |       +---virtual-fs
    |   |   |       |   \---host
    |   |   |       \---workspace
    |   |   |           \---json
    |   |   |               \---test
    |   |   |                   \---cases
    |   |   \---schematics
    |   |       +---node_modules
    |   |       |   \---rxjs
    |   |       |       +---add
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   \---operator
    |   |       |       +---ajax
    |   |       |       +---bundles
    |   |       |       +---fetch
    |   |       |       +---internal
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   +---operators
    |   |       |       |   +---scheduled
    |   |       |       |   +---scheduler
    |   |       |       |   +---symbol
    |   |       |       |   +---testing
    |   |       |       |   \---util
    |   |       |       +---internal-compatibility
    |   |       |       +---migrations
    |   |       |       |   \---update-6_0_0
    |   |       |       +---observable
    |   |       |       |   \---dom
    |   |       |       +---operator
    |   |       |       +---operators
    |   |       |       +---scheduler
    |   |       |       +---src
    |   |       |       |   +---add
    |   |       |       |   |   +---observable
    |   |       |       |   |   |   \---dom
    |   |       |       |   |   \---operator
    |   |       |       |   +---ajax
    |   |       |       |   +---fetch
    |   |       |       |   +---internal
    |   |       |       |   |   +---observable
    |   |       |       |   |   |   \---dom
    |   |       |       |   |   +---operators
    |   |       |       |   |   +---scheduled
    |   |       |       |   |   +---scheduler
    |   |       |       |   |   +---symbol
    |   |       |       |   |   +---testing
    |   |       |       |   |   \---util
    |   |       |       |   +---internal-compatibility
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   +---operator
    |   |       |       |   +---operators
    |   |       |       |   +---scheduler
    |   |       |       |   +---symbol
    |   |       |       |   +---testing
    |   |       |       |   +---util
    |   |       |       |   \---webSocket
    |   |       |       +---symbol
    |   |       |       +---testing
    |   |       |       +---util
    |   |       |       +---webSocket
    |   |       |       +---_esm2015
    |   |       |       |   +---ajax
    |   |       |       |   +---fetch
    |   |       |       |   +---internal
    |   |       |       |   |   +---observable
    |   |       |       |   |   |   \---dom
    |   |       |       |   |   +---operators
    |   |       |       |   |   +---scheduled
    |   |       |       |   |   +---scheduler
    |   |       |       |   |   +---symbol
    |   |       |       |   |   +---testing
    |   |       |       |   |   \---util
    |   |       |       |   +---internal-compatibility
    |   |       |       |   +---operators
    |   |       |       |   +---testing
    |   |       |       |   \---webSocket
    |   |       |       \---_esm5
    |   |       |           +---ajax
    |   |       |           +---fetch
    |   |       |           +---internal
    |   |       |           |   +---observable
    |   |       |           |   |   \---dom
    |   |       |           |   +---operators
    |   |       |           |   +---scheduled
    |   |       |           |   +---scheduler
    |   |       |           |   +---symbol
    |   |       |           |   +---testing
    |   |       |           |   \---util
    |   |       |           +---internal-compatibility
    |   |       |           +---operators
    |   |       |           +---testing
    |   |       |           \---webSocket
    |   |       +---src
    |   |       |   +---engine
    |   |       |   +---exception
    |   |       |   +---formats
    |   |       |   +---rules
    |   |       |   +---sink
    |   |       |   +---tree
    |   |       |   +---utility
    |   |       |   \---workflow
    |   |       +---tasks
    |   |       |   +---node
    |   |       |   +---package-manager
    |   |       |   +---repo-init
    |   |       |   +---run-schematic
    |   |       |   \---tslint-fix
    |   |       |       \---test
    |   |       |           \---rules
    |   |       +---testing
    |   |       \---tools
    |   |           \---workflow
    |   +---@babel
    |   |   +---code-frame
    |   |   |   \---lib
    |   |   +---compat-data
    |   |   |   \---data
    |   |   +---core
    |   |   |   +---lib
    |   |   |   |   +---config
    |   |   |   |   |   +---files
    |   |   |   |   |   +---helpers
    |   |   |   |   |   \---validation
    |   |   |   |   +---gensync-utils
    |   |   |   |   +---parser
    |   |   |   |   |   \---util
    |   |   |   |   +---tools
    |   |   |   |   \---transformation
    |   |   |   |       \---file
    |   |   |   +---node_modules
    |   |   |   |   +---debug
    |   |   |   |   |   \---src
    |   |   |   |   +---ms
    |   |   |   |   \---source-map
    |   |   |   |       +---dist
    |   |   |   |       \---lib
    |   |   |   \---src
    |   |   |       \---config
    |   |   |           \---files
    |   |   +---generator
    |   |   |   +---lib
    |   |   |   |   +---generators
    |   |   |   |   \---node
    |   |   |   \---node_modules
    |   |   |       +---.bin
    |   |   |       +---jsesc
    |   |   |       |   +---bin
    |   |   |       |   \---man
    |   |   |       \---source-map
    |   |   |           +---dist
    |   |   |           \---lib
    |   |   +---helper-annotate-as-pure
    |   |   |   \---lib
    |   |   +---helper-builder-binary-assignment-operator-visitor
    |   |   |   \---lib
    |   |   +---helper-compilation-targets
    |   |   |   \---lib
    |   |   +---helper-create-class-features-plugin
    |   |   |   \---lib
    |   |   +---helper-create-regexp-features-plugin
    |   |   |   \---lib
    |   |   +---helper-define-map
    |   |   |   \---lib
    |   |   +---helper-explode-assignable-expression
    |   |   |   \---lib
    |   |   +---helper-function-name
    |   |   |   \---lib
    |   |   +---helper-get-function-arity
    |   |   |   \---lib
    |   |   +---helper-hoist-variables
    |   |   |   \---lib
    |   |   +---helper-member-expression-to-functions
    |   |   |   \---lib
    |   |   +---helper-module-imports
    |   |   |   \---lib
    |   |   +---helper-module-transforms
    |   |   |   \---lib
    |   |   +---helper-optimise-call-expression
    |   |   |   \---lib
    |   |   +---helper-plugin-utils
    |   |   |   \---lib
    |   |   +---helper-remap-async-to-generator
    |   |   |   \---lib
    |   |   +---helper-replace-supers
    |   |   |   \---lib
    |   |   +---helper-simple-access
    |   |   |   \---lib
    |   |   +---helper-skip-transparent-expression-wrappers
    |   |   |   \---lib
    |   |   +---helper-split-export-declaration
    |   |   |   \---lib
    |   |   +---helper-validator-identifier
    |   |   |   +---lib
    |   |   |   \---scripts
    |   |   +---helper-validator-option
    |   |   |   \---lib
    |   |   +---helper-wrap-function
    |   |   |   \---lib
    |   |   +---helpers
    |   |   |   \---lib
    |   |   +---highlight
    |   |   |   +---lib
    |   |   |   \---node_modules
    |   |   |       \---js-tokens
    |   |   +---parser
    |   |   |   +---bin
    |   |   |   +---lib
    |   |   |   \---typings
    |   |   +---plugin-proposal-async-generator-functions
    |   |   |   \---lib
    |   |   +---plugin-proposal-class-properties
    |   |   |   \---lib
    |   |   +---plugin-proposal-dynamic-import
    |   |   |   \---lib
    |   |   +---plugin-proposal-export-namespace-from
    |   |   |   \---lib
    |   |   +---plugin-proposal-json-strings
    |   |   |   \---lib
    |   |   +---plugin-proposal-logical-assignment-operators
    |   |   |   \---lib
    |   |   +---plugin-proposal-nullish-coalescing-operator
    |   |   |   \---lib
    |   |   +---plugin-proposal-numeric-separator
    |   |   |   \---lib
    |   |   +---plugin-proposal-object-rest-spread
    |   |   |   \---lib
    |   |   +---plugin-proposal-optional-catch-binding
    |   |   |   \---lib
    |   |   +---plugin-proposal-optional-chaining
    |   |   |   \---lib
    |   |   +---plugin-proposal-private-methods
    |   |   |   \---lib
    |   |   +---plugin-proposal-unicode-property-regex
    |   |   |   \---lib
    |   |   +---plugin-syntax-async-generators
    |   |   |   \---lib
    |   |   +---plugin-syntax-class-properties
    |   |   |   \---lib
    |   |   +---plugin-syntax-dynamic-import
    |   |   |   \---lib
    |   |   +---plugin-syntax-export-namespace-from
    |   |   |   \---lib
    |   |   +---plugin-syntax-json-strings
    |   |   |   \---lib
    |   |   +---plugin-syntax-logical-assignment-operators
    |   |   |   \---lib
    |   |   +---plugin-syntax-nullish-coalescing-operator
    |   |   |   \---lib
    |   |   +---plugin-syntax-numeric-separator
    |   |   |   \---lib
    |   |   +---plugin-syntax-object-rest-spread
    |   |   |   \---lib
    |   |   +---plugin-syntax-optional-catch-binding
    |   |   |   \---lib
    |   |   +---plugin-syntax-optional-chaining
    |   |   |   \---lib
    |   |   +---plugin-syntax-top-level-await
    |   |   |   \---lib
    |   |   +---plugin-transform-arrow-functions
    |   |   |   \---lib
    |   |   +---plugin-transform-async-to-generator
    |   |   |   \---lib
    |   |   +---plugin-transform-block-scoped-functions
    |   |   |   \---lib
    |   |   +---plugin-transform-block-scoping
    |   |   |   \---lib
    |   |   +---plugin-transform-classes
    |   |   |   \---lib
    |   |   +---plugin-transform-computed-properties
    |   |   |   \---lib
    |   |   +---plugin-transform-destructuring
    |   |   |   \---lib
    |   |   +---plugin-transform-dotall-regex
    |   |   |   \---lib
    |   |   +---plugin-transform-duplicate-keys
    |   |   |   \---lib
    |   |   +---plugin-transform-exponentiation-operator
    |   |   |   \---lib
    |   |   +---plugin-transform-for-of
    |   |   |   \---lib
    |   |   +---plugin-transform-function-name
    |   |   |   \---lib
    |   |   +---plugin-transform-literals
    |   |   |   \---lib
    |   |   +---plugin-transform-member-expression-literals
    |   |   |   \---lib
    |   |   +---plugin-transform-modules-amd
    |   |   |   \---lib
    |   |   +---plugin-transform-modules-commonjs
    |   |   |   \---lib
    |   |   +---plugin-transform-modules-systemjs
    |   |   |   \---lib
    |   |   +---plugin-transform-modules-umd
    |   |   |   \---lib
    |   |   +---plugin-transform-named-capturing-groups-regex
    |   |   |   \---lib
    |   |   +---plugin-transform-new-target
    |   |   |   \---lib
    |   |   +---plugin-transform-object-super
    |   |   |   \---lib
    |   |   +---plugin-transform-parameters
    |   |   |   \---lib
    |   |   +---plugin-transform-property-literals
    |   |   |   \---lib
    |   |   +---plugin-transform-regenerator
    |   |   |   \---lib
    |   |   +---plugin-transform-reserved-words
    |   |   |   \---lib
    |   |   +---plugin-transform-runtime
    |   |   |   +---lib
    |   |   |   |   \---get-runtime-path
    |   |   |   \---src
    |   |   |       \---get-runtime-path
    |   |   +---plugin-transform-shorthand-properties
    |   |   |   \---lib
    |   |   +---plugin-transform-spread
    |   |   |   \---lib
    |   |   +---plugin-transform-sticky-regex
    |   |   |   \---lib
    |   |   +---plugin-transform-template-literals
    |   |   |   \---lib
    |   |   +---plugin-transform-typeof-symbol
    |   |   |   \---lib
    |   |   +---plugin-transform-unicode-escapes
    |   |   |   \---lib
    |   |   +---plugin-transform-unicode-regex
    |   |   |   \---lib
    |   |   +---preset-env
    |   |   |   +---data
    |   |   |   \---lib
    |   |   |       \---polyfills
    |   |   |           +---corejs2
    |   |   |           +---corejs3
    |   |   |           \---regenerator
    |   |   +---preset-modules
    |   |   |   +---lib
    |   |   |   |   \---plugins
    |   |   |   |       +---transform-async-arrows-in-class
    |   |   |   |       +---transform-edge-default-parameters
    |   |   |   |       +---transform-edge-function-name
    |   |   |   |       +---transform-jsx-spread
    |   |   |   |       +---transform-safari-block-shadowing
    |   |   |   |       +---transform-safari-for-shadowing
    |   |   |   |       \---transform-tagged-template-caching
    |   |   |   \---src
    |   |   |       \---plugins
    |   |   |           +---transform-async-arrows-in-class
    |   |   |           +---transform-edge-default-parameters
    |   |   |           +---transform-edge-function-name
    |   |   |           +---transform-jsx-spread
    |   |   |           +---transform-safari-block-shadowing
    |   |   |           +---transform-safari-for-shadowing
    |   |   |           \---transform-tagged-template-caching
    |   |   +---runtime
    |   |   |   +---helpers
    |   |   |   |   \---esm
    |   |   |   \---regenerator
    |   |   +---template
    |   |   |   \---lib
    |   |   +---traverse
    |   |   |   +---lib
    |   |   |   |   +---path
    |   |   |   |   |   +---inference
    |   |   |   |   |   \---lib
    |   |   |   |   \---scope
    |   |   |   |       \---lib
    |   |   |   \---node_modules
    |   |   |       +---debug
    |   |   |       |   \---src
    |   |   |       +---globals
    |   |   |       \---ms
    |   |   \---types
    |   |       +---lib
    |   |       |   +---asserts
    |   |       |   |   \---generated
    |   |       |   +---ast-types
    |   |       |   |   \---generated
    |   |       |   +---builders
    |   |       |   |   +---flow
    |   |       |   |   +---generated
    |   |       |   |   +---react
    |   |       |   |   \---typescript
    |   |       |   +---clone
    |   |       |   +---comments
    |   |       |   +---constants
    |   |       |   |   \---generated
    |   |       |   +---converters
    |   |       |   +---definitions
    |   |       |   +---modifications
    |   |       |   |   +---flow
    |   |       |   |   \---typescript
    |   |       |   +---retrievers
    |   |       |   +---traverse
    |   |       |   +---utils
    |   |       |   |   \---react
    |   |       |   \---validators
    |   |       |       +---generated
    |   |       |       \---react
    |   |       +---node_modules
    |   |       |   \---to-fast-properties
    |   |       \---scripts
    |   |           +---generators
    |   |           \---utils
    |   +---@istanbuljs
    |   |   \---schema
    |   +---@jsdevtools
    |   |   \---coverage-istanbul-loader
    |   |       \---lib
    |   +---@ngtools
    |   |   \---webpack
    |   |       +---node_modules
    |   |       |   +---@angular-devkit
    |   |       |   |   \---core
    |   |       |   |       +---node
    |   |       |   |       |   +---experimental
    |   |       |   |       |   |   \---jobs
    |   |       |   |       |   \---testing
    |   |       |   |       \---src
    |   |       |   |           +---analytics
    |   |       |   |           +---exception
    |   |       |   |           +---experimental
    |   |       |   |           |   \---jobs
    |   |       |   |           +---json
    |   |       |   |           |   \---schema
    |   |       |   |           +---logger
    |   |       |   |           +---utils
    |   |       |   |           +---virtual-fs
    |   |       |   |           |   \---host
    |   |       |   |           \---workspace
    |   |       |   |               \---json
    |   |       |   |                   \---test
    |   |       |   |                       \---cases
    |   |       |   +---ajv
    |   |       |   |   +---dist
    |   |       |   |   +---lib
    |   |       |   |   |   +---compile
    |   |       |   |   |   +---dot
    |   |       |   |   |   +---dotjs
    |   |       |   |   |   \---refs
    |   |       |   |   \---scripts
    |   |       |   +---fast-deep-equal
    |   |       |   |   \---es6
    |   |       |   +---fast-json-stable-stringify
    |   |       |   |   +---.github
    |   |       |   |   +---benchmark
    |   |       |   |   +---example
    |   |       |   |   \---test
    |   |       |   \---rxjs
    |   |       |       +---add
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   \---operator
    |   |       |       +---ajax
    |   |       |       +---bundles
    |   |       |       +---fetch
    |   |       |       +---internal
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   +---operators
    |   |       |       |   +---scheduled
    |   |       |       |   +---scheduler
    |   |       |       |   +---symbol
    |   |       |       |   +---testing
    |   |       |       |   \---util
    |   |       |       +---internal-compatibility
    |   |       |       +---migrations
    |   |       |       |   \---update-6_0_0
    |   |       |       +---observable
    |   |       |       |   \---dom
    |   |       |       +---operator
    |   |       |       +---operators
    |   |       |       +---scheduler
    |   |       |       +---src
    |   |       |       |   +---add
    |   |       |       |   |   +---observable
    |   |       |       |   |   |   \---dom
    |   |       |       |   |   \---operator
    |   |       |       |   +---ajax
    |   |       |       |   +---fetch
    |   |       |       |   +---internal
    |   |       |       |   |   +---observable
    |   |       |       |   |   |   \---dom
    |   |       |       |   |   +---operators
    |   |       |       |   |   +---scheduled
    |   |       |       |   |   +---scheduler
    |   |       |       |   |   +---symbol
    |   |       |       |   |   +---testing
    |   |       |       |   |   \---util
    |   |       |       |   +---internal-compatibility
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   +---operator
    |   |       |       |   +---operators
    |   |       |       |   +---scheduler
    |   |       |       |   +---symbol
    |   |       |       |   +---testing
    |   |       |       |   +---util
    |   |       |       |   \---webSocket
    |   |       |       +---symbol
    |   |       |       +---testing
    |   |       |       +---util
    |   |       |       +---webSocket
    |   |       |       +---_esm2015
    |   |       |       |   +---ajax
    |   |       |       |   +---fetch
    |   |       |       |   +---internal
    |   |       |       |   |   +---observable
    |   |       |       |   |   |   \---dom
    |   |       |       |   |   +---operators
    |   |       |       |   |   +---scheduled
    |   |       |       |   |   +---scheduler
    |   |       |       |   |   +---symbol
    |   |       |       |   |   +---testing
    |   |       |       |   |   \---util
    |   |       |       |   +---internal-compatibility
    |   |       |       |   +---operators
    |   |       |       |   +---testing
    |   |       |       |   \---webSocket
    |   |       |       \---_esm5
    |   |       |           +---ajax
    |   |       |           +---fetch
    |   |       |           +---internal
    |   |       |           |   +---observable
    |   |       |           |   |   \---dom
    |   |       |           |   +---operators
    |   |       |           |   +---scheduled
    |   |       |           |   +---scheduler
    |   |       |           |   +---symbol
    |   |       |           |   +---testing
    |   |       |           |   \---util
    |   |       |           +---internal-compatibility
    |   |       |           +---operators
    |   |       |           +---testing
    |   |       |           \---webSocket
    |   |       \---src
    |   |           +---ivy
    |   |           \---transformers
    |   +---@nodelib
    |   |   +---fs.scandir
    |   |   |   +---out
    |   |   |   |   +---adapters
    |   |   |   |   +---providers
    |   |   |   |   +---types
    |   |   |   |   \---utils
    |   |   |   \---src
    |   |   |       +---adapters
    |   |   |       +---providers
    |   |   |       +---types
    |   |   |       \---utils
    |   |   +---fs.stat
    |   |   |   +---out
    |   |   |   |   +---adapters
    |   |   |   |   +---providers
    |   |   |   |   \---types
    |   |   |   \---src
    |   |   |       +---adapters
    |   |   |       +---providers
    |   |   |       \---types
    |   |   \---fs.walk
    |   |       +---out
    |   |       |   +---providers
    |   |       |   +---readers
    |   |       |   +---tests
    |   |       |   \---types
    |   |       \---src
    |   |           +---providers
    |   |           +---readers
    |   |           +---tests
    |   |           \---types
    |   +---@npmcli
    |   |   +---ci-detect
    |   |   +---git
    |   |   |   +---lib
    |   |   |   \---node_modules
    |   |   |       +---.bin
    |   |   |       +---lru-cache
    |   |   |       +---mkdirp
    |   |   |       |   +---bin
    |   |   |       |   \---lib
    |   |   |       +---semver
    |   |   |       |   +---bin
    |   |   |       |   +---classes
    |   |   |       |   +---functions
    |   |   |       |   +---internal
    |   |   |       |   \---ranges
    |   |   |       +---which
    |   |   |       |   \---bin
    |   |   |       \---yallist
    |   |   +---installed-package-contents
    |   |   +---move-file
    |   |   |   \---node_modules
    |   |   |       +---.bin
    |   |   |       \---mkdirp
    |   |   |           +---bin
    |   |   |           \---lib
    |   |   +---node-gyp
    |   |   |   \---lib
    |   |   +---promise-spawn
    |   |   \---run-script
    |   |       \---lib
    |   |           \---node-gyp-bin
    |   +---@schematics
    |   |   +---angular
    |   |   |   +---app-shell
    |   |   |   +---application
    |   |   |   |   +---files
    |   |   |   |   |   \---src
    |   |   |   |   |       +---assets
    |   |   |   |   |       \---environments
    |   |   |   |   \---other-files
    |   |   |   +---class
    |   |   |   |   \---files
    |   |   |   +---component
    |   |   |   |   \---files
    |   |   |   |       \---__name@dasherize@if-flat__
    |   |   |   +---directive
    |   |   |   |   \---files
    |   |   |   |       \---__name@dasherize@if-flat__
    |   |   |   +---e2e
    |   |   |   |   \---files
    |   |   |   |       \---src
    |   |   |   +---enum
    |   |   |   |   \---files
    |   |   |   +---guard
    |   |   |   |   \---files
    |   |   |   +---interceptor
    |   |   |   |   \---files
    |   |   |   |       \---__name@dasherize@if-flat__
    |   |   |   +---interface
    |   |   |   |   \---files
    |   |   |   +---library
    |   |   |   |   \---files
    |   |   |   |       \---src
    |   |   |   +---migrations
    |   |   |   |   +---update-10
    |   |   |   |   +---update-11
    |   |   |   |   +---update-6
    |   |   |   |   +---update-7
    |   |   |   |   +---update-8
    |   |   |   |   \---update-9
    |   |   |   +---module
    |   |   |   |   \---files
    |   |   |   |       \---__name@dasherize@if-flat__
    |   |   |   +---ng-new
    |   |   |   +---pipe
    |   |   |   |   \---files
    |   |   |   |       \---__name@dasherize@if-flat__
    |   |   |   +---resolver
    |   |   |   |   \---files
    |   |   |   +---service
    |   |   |   |   \---files
    |   |   |   |       \---__name@dasherize@if-flat__
    |   |   |   +---service-worker
    |   |   |   |   \---files
    |   |   |   +---third_party
    |   |   |   |   \---github.com
    |   |   |   |       \---Microsoft
    |   |   |   |           \---TypeScript
    |   |   |   |               \---lib
    |   |   |   +---universal
    |   |   |   |   \---files
    |   |   |   |       +---root
    |   |   |   |       \---src
    |   |   |   |           \---app
    |   |   |   +---utility
    |   |   |   |   \---test
    |   |   |   +---web-worker
    |   |   |   |   \---files
    |   |   |   |       +---worker
    |   |   |   |       \---worker-tsconfig
    |   |   |   \---workspace
    |   |   |       \---files
    |   |   \---update
    |   |       +---migrate
    |   |       |   \---test
    |   |       +---node_modules
    |   |       |   +---.bin
    |   |       |   +---lru-cache
    |   |       |   +---semver
    |   |       |   |   +---bin
    |   |       |   |   +---classes
    |   |       |   |   +---functions
    |   |       |   |   +---internal
    |   |       |   |   \---ranges
    |   |       |   \---yallist
    |   |       \---update
    |   +---@tootallnate
    |   |   \---once
    |   |       \---dist
    |   +---@types
    |   |   +---component-emitter
    |   |   +---cookie
    |   |   +---cors
    |   |   +---glob
    |   |   +---jasmine
    |   |   +---jasminewd2
    |   |   +---json-schema
    |   |   +---minimatch
    |   |   +---node
    |   |   +---parse-json
    |   |   +---q
    |   |   +---selenium-webdriver
    |   |   +---source-list-map
    |   |   \---webpack-sources
    |   |       \---node_modules
    |   |           \---source-map
    |   |               +---dist
    |   |               \---lib
    |   +---@webassemblyjs
    |   |   +---ast
    |   |   |   +---esm
    |   |   |   |   +---transform
    |   |   |   |   |   +---denormalize-type-references
    |   |   |   |   |   \---wast-identifier-to-index
    |   |   |   |   \---types
    |   |   |   +---lib
    |   |   |   |   +---transform
    |   |   |   |   |   +---denormalize-type-references
    |   |   |   |   |   \---wast-identifier-to-index
    |   |   |   |   \---types
    |   |   |   \---scripts
    |   |   +---floating-point-hex-parser
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---helper-api-error
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---helper-buffer
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---helper-code-frame
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---helper-fsm
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---helper-module-context
    |   |   |   +---esm
    |   |   |   +---lib
    |   |   |   +---src
    |   |   |   \---test
    |   |   +---helper-wasm-bytecode
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---helper-wasm-section
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---ieee754
    |   |   |   +---esm
    |   |   |   +---lib
    |   |   |   \---src
    |   |   +---leb128
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---utf8
    |   |   |   +---esm
    |   |   |   +---lib
    |   |   |   +---src
    |   |   |   \---test
    |   |   +---wasm-edit
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---wasm-gen
    |   |   |   +---esm
    |   |   |   |   \---encoder
    |   |   |   \---lib
    |   |   |       \---encoder
    |   |   +---wasm-opt
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   +---wasm-parser
    |   |   |   +---esm
    |   |   |   |   \---types
    |   |   |   \---lib
    |   |   |       \---types
    |   |   +---wast-parser
    |   |   |   +---esm
    |   |   |   \---lib
    |   |   \---wast-printer
    |   |       +---esm
    |   |       \---lib
    |   +---@xtuc
    |   |   +---ieee754
    |   |   |   \---dist
    |   |   \---long
    |   |       +---dist
    |   |       \---src
    |   +---@yarnpkg
    |   |   \---lockfile
    |   +---abab
    |   |   \---lib
    |   +---abbrev
    |   +---accepts
    |   +---acorn
    |   |   +---bin
    |   |   \---dist
    |   +---adjust-sourcemap-loader
    |   |   +---codec
    |   |   |   \---utility
    |   |   \---lib
    |   |       \---process
    |   +---adm-zip
    |   |   +---headers
    |   |   +---methods
    |   |   \---util
    |   +---agent-base
    |   |   +---dist
    |   |   |   \---src
    |   |   +---node_modules
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   \---ms
    |   |   \---src
    |   +---agentkeepalive
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---aggregate-error
    |   +---ajv
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---compile
    |   |   |   +---dot
    |   |   |   +---dotjs
    |   |   |   \---refs
    |   |   \---scripts
    |   +---ajv-errors
    |   |   \---lib
    |   |       +---dot
    |   |       \---dotjs
    |   +---ajv-keywords
    |   |   \---keywords
    |   |       +---dot
    |   |       \---dotjs
    |   +---alphanum-sort
    |   |   \---lib
    |   +---ansi-colors
    |   |   \---types
    |   +---ansi-escapes
    |   +---ansi-html
    |   |   \---bin
    |   +---ansi-regex
    |   +---ansi-styles
    |   +---anymatch
    |   +---app-root-path
    |   |   \---lib
    |   +---append-transform
    |   +---aproba
    |   +---are-we-there-yet
    |   +---argparse
    |   |   \---lib
    |   |       +---action
    |   |       |   +---append
    |   |       |   \---store
    |   |       +---argument
    |   |       \---help
    |   +---arity-n
    |   +---arr-diff
    |   +---arr-flatten
    |   +---arr-union
    |   +---array-flatten
    |   +---array-union
    |   +---array-uniq
    |   +---array-unique
    |   +---arrify
    |   +---asap
    |   +---asn1
    |   |   \---lib
    |   |       \---ber
    |   +---asn1.js
    |   |   +---lib
    |   |   |   \---asn1
    |   |   |       +---base
    |   |   |       +---constants
    |   |   |       +---decoders
    |   |   |       \---encoders
    |   |   \---node_modules
    |   |       \---bn.js
    |   |           +---lib
    |   |           \---util
    |   +---assert
    |   |   \---node_modules
    |   |       +---inherits
    |   |       \---util
    |   |           +---support
    |   |           \---test
    |   |               +---browser
    |   |               \---node
    |   +---assert-plus
    |   +---assign-symbols
    |   +---async
    |   |   +---dist
    |   |   \---internal
    |   +---async-each
    |   +---async-limiter
    |   +---asynckit
    |   |   \---lib
    |   +---atob
    |   |   \---bin
    |   +---autoprefixer
    |   |   +---bin
    |   |   +---data
    |   |   \---lib
    |   |       \---hacks
    |   +---aws-sign2
    |   +---aws4
    |   |   \---.github
    |   +---babel-code-frame
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---ansi-styles
    |   |       +---chalk
    |   |       \---supports-color
    |   +---babel-loader
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---json5
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       +---loader-utils
    |   |       |   \---lib
    |   |       +---make-dir
    |   |       \---semver
    |   |           \---bin
    |   +---babel-plugin-dynamic-import-node
    |   |   \---lib
    |   +---balanced-match
    |   +---base
    |   |   \---node_modules
    |   |       +---define-property
    |   |       +---is-accessor-descriptor
    |   |       +---is-data-descriptor
    |   |       \---is-descriptor
    |   +---base64-arraybuffer
    |   |   \---lib
    |   +---base64-js
    |   +---base64id
    |   |   \---lib
    |   +---batch
    |   +---bcrypt-pbkdf
    |   +---big.js
    |   +---binary-extensions
    |   +---bindings
    |   +---bl
    |   |   +---node_modules
    |   |   |   \---readable-stream
    |   |   |       \---lib
    |   |   |           \---internal
    |   |   |               \---streams
    |   |   \---test
    |   +---blocking-proxy
    |   |   +---built
    |   |   |   \---lib
    |   |   |       \---client_scripts
    |   |   \---examples
    |   +---bluebird
    |   |   \---js
    |   |       +---browser
    |   |       \---release
    |   +---bn.js
    |   |   \---lib
    |   +---body-parser
    |   |   +---lib
    |   |   |   \---types
    |   |   \---node_modules
    |   |       +---bytes
    |   |       \---qs
    |   |           +---dist
    |   |           +---lib
    |   |           \---test
    |   +---bonjour
    |   |   +---lib
    |   |   \---test
    |   +---boolbase
    |   +---brace-expansion
    |   +---braces
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---extend-shallow
    |   +---brorand
    |   |   \---test
    |   +---browserify-aes
    |   |   \---modes
    |   +---browserify-cipher
    |   +---browserify-des
    |   +---browserify-rsa
    |   +---browserify-sign
    |   |   +---browser
    |   |   \---node_modules
    |   |       +---readable-stream
    |   |       |   \---lib
    |   |       |       \---internal
    |   |       |           \---streams
    |   |       \---safe-buffer
    |   +---browserify-zlib
    |   |   +---lib
    |   |   \---src
    |   +---browserslist
    |   +---browserstack
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   +---agent-base
    |   |   |   |   \---test
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   +---https-proxy-agent
    |   |   |   \---ms
    |   |   \---test
    |   +---buffer
    |   +---buffer-from
    |   +---buffer-indexof
    |   |   \---test
    |   +---buffer-xor
    |   |   \---test
    |   +---builtin-modules
    |   +---builtin-status-codes
    |   +---builtins
    |   +---bytes
    |   +---cacache
    |   |   +---lib
    |   |   |   +---content
    |   |   |   \---util
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---chownr
    |   |       +---fs-minipass
    |   |       +---glob
    |   |       +---lru-cache
    |   |       +---minipass
    |   |       +---mkdirp
    |   |       |   +---bin
    |   |       |   \---lib
    |   |       +---rimraf
    |   |       +---ssri
    |   |       \---yallist
    |   +---cache-base
    |   +---call-bind
    |   |   +---.github
    |   |   \---test
    |   +---caller-callsite
    |   +---caller-path
    |   +---callsites
    |   +---camelcase
    |   +---caniuse-api
    |   |   \---dist
    |   +---caniuse-lite
    |   |   +---data
    |   |   |   +---features
    |   |   |   \---regions
    |   |   \---dist
    |   |       +---lib
    |   |       \---unpacker
    |   +---canonical-path
    |   +---caseless
    |   +---chalk
    |   |   +---node_modules
    |   |   |   \---supports-color
    |   |   \---types
    |   +---chardet
    |   |   \---encoding
    |   +---chokidar
    |   |   \---lib
    |   +---chownr
    |   +---chrome-trace-event
    |   |   \---dist
    |   +---cipher-base
    |   +---circular-dependency-plugin
    |   +---class-utils
    |   |   \---node_modules
    |   |       \---define-property
    |   +---clean-stack
    |   +---cli-cursor
    |   +---cli-spinners
    |   +---cli-width
    |   |   \---.nyc_output
    |   |       \---processinfo
    |   +---cliui
    |   |   \---node_modules
    |   |       +---ansi-regex
    |   |       +---is-fullwidth-code-point
    |   |       +---string-width
    |   |       +---strip-ansi
    |   |       \---wrap-ansi
    |   +---clone
    |   +---clone-deep
    |   +---coa
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---@types
    |   |           \---q
    |   +---code-point-at
    |   +---codelyzer
    |   |   +---angular
    |   |   |   +---fileResolver
    |   |   |   +---styles
    |   |   |   +---templates
    |   |   |   \---urlResolvers
    |   |   +---node_modules
    |   |   |   +---source-map
    |   |   |   |   +---dist
    |   |   |   |   \---lib
    |   |   |   \---sprintf-js
    |   |   |       +---dist
    |   |   |       \---src
    |   |   +---util
    |   |   \---walkerFactory
    |   +---collection-visit
    |   +---color
    |   +---color-convert
    |   +---color-name
    |   +---color-string
    |   +---colorette
    |   +---colors
    |   |   +---examples
    |   |   +---lib
    |   |   |   +---custom
    |   |   |   +---maps
    |   |   |   \---system
    |   |   \---themes
    |   +---combined-stream
    |   |   \---lib
    |   +---commander
    |   |   \---typings
    |   +---commondir
    |   |   +---example
    |   |   \---test
    |   +---compare-versions
    |   +---component-emitter
    |   +---compose-function
    |   |   \---module
    |   +---compressible
    |   +---compression
    |   +---concat-map
    |   |   +---example
    |   |   \---test
    |   +---concat-stream
    |   +---connect
    |   +---connect-history-api-fallback
    |   |   \---lib
    |   +---console-browserify
    |   |   \---test
    |   |       \---static
    |   +---console-control-strings
    |   +---constants-browserify
    |   +---content-disposition
    |   +---content-type
    |   +---convert-source-map
    |   +---cookie
    |   +---cookie-signature
    |   +---copy-anything
    |   |   +---.github
    |   |   +---build
    |   |   +---dist
    |   |   +---src
    |   |   +---test
    |   |   \---types
    |   +---copy-concurrently
    |   +---copy-descriptor
    |   +---copy-webpack-plugin
    |   |   +---dist
    |   |   |   \---utils
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---glob-parent
    |   |       +---normalize-path
    |   |       +---p-limit
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       \---webpack-sources
    |   |           \---lib
    |   +---core-js
    |   |   +---build
    |   |   +---client
    |   |   +---core
    |   |   +---es5
    |   |   +---es6
    |   |   +---es7
    |   |   +---fn
    |   |   |   +---array
    |   |   |   |   \---virtual
    |   |   |   +---date
    |   |   |   +---dom-collections
    |   |   |   +---error
    |   |   |   +---function
    |   |   |   |   \---virtual
    |   |   |   +---json
    |   |   |   +---map
    |   |   |   +---math
    |   |   |   +---number
    |   |   |   |   \---virtual
    |   |   |   +---object
    |   |   |   +---promise
    |   |   |   +---reflect
    |   |   |   +---regexp
    |   |   |   +---set
    |   |   |   +---string
    |   |   |   |   \---virtual
    |   |   |   +---symbol
    |   |   |   +---system
    |   |   |   +---typed
    |   |   |   +---weak-map
    |   |   |   \---weak-set
    |   |   +---library
    |   |   |   +---core
    |   |   |   +---es5
    |   |   |   +---es6
    |   |   |   +---es7
    |   |   |   +---fn
    |   |   |   |   +---array
    |   |   |   |   |   \---virtual
    |   |   |   |   +---date
    |   |   |   |   +---dom-collections
    |   |   |   |   +---error
    |   |   |   |   +---function
    |   |   |   |   |   \---virtual
    |   |   |   |   +---json
    |   |   |   |   +---map
    |   |   |   |   +---math
    |   |   |   |   +---number
    |   |   |   |   |   \---virtual
    |   |   |   |   +---object
    |   |   |   |   +---promise
    |   |   |   |   +---reflect
    |   |   |   |   +---regexp
    |   |   |   |   +---set
    |   |   |   |   +---string
    |   |   |   |   |   \---virtual
    |   |   |   |   +---symbol
    |   |   |   |   +---system
    |   |   |   |   +---typed
    |   |   |   |   +---weak-map
    |   |   |   |   \---weak-set
    |   |   |   +---modules
    |   |   |   +---stage
    |   |   |   \---web
    |   |   +---modules
    |   |   |   \---library
    |   |   +---stage
    |   |   \---web
    |   +---core-js-compat
    |   |   \---node_modules
    |   |       +---.bin
    |   |       \---semver
    |   |           +---bin
    |   |           +---classes
    |   |           +---functions
    |   |           +---internal
    |   |           \---ranges
    |   +---core-util-is
    |   |   \---lib
    |   +---cors
    |   |   \---lib
    |   +---cosmiconfig
    |   |   \---dist
    |   +---create-ecdh
    |   |   \---node_modules
    |   |       \---bn.js
    |   |           +---lib
    |   |           \---util
    |   +---create-hash
    |   +---create-hmac
    |   +---critters
    |   |   +---dist
    |   |   +---node_modules
    |   |   |   +---ansi-styles
    |   |   |   +---chalk
    |   |   |   |   \---source
    |   |   |   +---color-convert
    |   |   |   +---color-name
    |   |   |   +---has-flag
    |   |   |   \---supports-color
    |   |   \---src
    |   +---cross-spawn
    |   |   \---lib
    |   |       \---util
    |   +---crypto-browserify
    |   |   +---example
    |   |   \---test
    |   |       \---node
    |   +---css
    |   |   +---lib
    |   |   |   +---parse
    |   |   |   \---stringify
    |   |   \---node_modules
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       \---source-map-resolve
    |   +---css-color-names
    |   +---css-declaration-sorter
    |   |   +---node_modules
    |   |   |   +---postcss
    |   |   |   |   +---docs
    |   |   |   |   |   \---guidelines
    |   |   |   |   \---lib
    |   |   |   \---source-map
    |   |   |       +---dist
    |   |   |       \---lib
    |   |   +---orders
    |   |   \---src
    |   +---css-loader
    |   |   +---dist
    |   |   |   +---plugins
    |   |   |   \---runtime
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---lru-cache
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       +---semver
    |   |       |   +---bin
    |   |       |   +---classes
    |   |       |   +---functions
    |   |       |   +---internal
    |   |       |   \---ranges
    |   |       \---yallist
    |   +---css-parse
    |   |   \---node_modules
    |   |       +---css
    |   |       |   \---lib
    |   |       |       +---parse
    |   |       |       \---stringify
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---css-select
    |   |   \---lib
    |   +---css-select-base-adapter
    |   |   \---test
    |   +---css-selector-tokenizer
    |   |   \---lib
    |   +---css-tree
    |   |   +---data
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---common
    |   |   |   +---convertor
    |   |   |   +---definition-syntax
    |   |   |   +---generator
    |   |   |   +---lexer
    |   |   |   +---parser
    |   |   |   +---syntax
    |   |   |   |   +---atrule
    |   |   |   |   +---config
    |   |   |   |   +---function
    |   |   |   |   +---node
    |   |   |   |   +---pseudo
    |   |   |   |   |   \---common
    |   |   |   |   \---scope
    |   |   |   +---tokenizer
    |   |   |   +---utils
    |   |   |   \---walker
    |   |   \---node_modules
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---css-what
    |   |   \---lib
    |   +---cssauron
    |   |   \---test
    |   +---cssesc
    |   |   +---bin
    |   |   \---man
    |   +---cssnano
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---cssnano-preset-default
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---cssnano-util-get-arguments
    |   |   \---dist
    |   +---cssnano-util-get-match
    |   |   \---dist
    |   +---cssnano-util-raw-cache
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---cssnano-util-same-parent
    |   |   \---dist
    |   +---csso
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---clean
    |   |   |   +---replace
    |   |   |   |   +---atrule
    |   |   |   |   \---property
    |   |   |   \---restructure
    |   |   |       \---prepare
    |   |   \---node_modules
    |   |       +---css-tree
    |   |       |   +---data
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       |       +---common
    |   |       |       +---convertor
    |   |       |       +---definition-syntax
    |   |       |       +---generator
    |   |       |       +---lexer
    |   |       |       +---parser
    |   |       |       +---syntax
    |   |       |       |   +---atrule
    |   |       |       |   +---config
    |   |       |       |   +---function
    |   |       |       |   +---node
    |   |       |       |   +---pseudo
    |   |       |       |   |   \---common
    |   |       |       |   \---scope
    |   |       |       +---tokenizer
    |   |       |       +---utils
    |   |       |       \---walker
    |   |       +---mdn-data
    |   |       |   +---api
    |   |       |   +---css
    |   |       |   \---l10n
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---custom-event
    |   |   \---test
    |   +---cyclist
    |   +---d
    |   |   +---.github
    |   |   \---test
    |   +---dashdash
    |   |   +---etc
    |   |   \---lib
    |   +---date-format
    |   |   +---lib
    |   |   \---test
    |   +---debug
    |   |   \---src
    |   +---debuglog
    |   +---decamelize
    |   +---decode-uri-component
    |   +---deep-equal
    |   |   +---example
    |   |   +---lib
    |   |   \---test
    |   +---default-gateway
    |   +---default-require-extensions
    |   |   \---node_modules
    |   |       \---strip-bom
    |   +---defaults
    |   +---define-properties
    |   |   \---test
    |   +---define-property
    |   |   \---node_modules
    |   |       +---is-accessor-descriptor
    |   |       +---is-data-descriptor
    |   |       \---is-descriptor
    |   +---del
    |   |   \---node_modules
    |   |       +---globby
    |   |       |   \---node_modules
    |   |       |       \---pify
    |   |       +---is-path-cwd
    |   |       +---is-path-in-cwd
    |   |       +---is-path-inside
    |   |       \---p-map
    |   +---delayed-stream
    |   |   \---lib
    |   +---delegates
    |   |   \---test
    |   +---depd
    |   |   \---lib
    |   |       +---browser
    |   |       \---compat
    |   +---dependency-graph
    |   |   +---lib
    |   |   \---specs
    |   +---des.js
    |   |   +---lib
    |   |   |   \---des
    |   |   \---test
    |   +---destroy
    |   +---detect-node
    |   +---dezalgo
    |   |   \---test
    |   +---di
    |   |   \---lib
    |   +---diff
    |   |   +---dist
    |   |   \---lib
    |   |       +---convert
    |   |       +---diff
    |   |       +---patch
    |   |       \---util
    |   +---diffie-hellman
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---bn.js
    |   |           +---lib
    |   |           \---util
    |   +---dir-glob
    |   +---dns-equal
    |   +---dns-packet
    |   +---dns-txt
    |   +---dom-serialize
    |   |   \---test
    |   +---dom-serializer
    |   |   \---node_modules
    |   |       \---domelementtype
    |   |           \---lib
    |   +---domain-browser
    |   |   \---source
    |   +---domelementtype
    |   +---domutils
    |   |   +---lib
    |   |   \---test
    |   |       \---tests
    |   +---dot-prop
    |   +---duplexify
    |   +---ecc-jsbn
    |   |   \---lib
    |   +---ee-first
    |   +---electron-to-chromium
    |   +---elliptic
    |   |   +---lib
    |   |   |   \---elliptic
    |   |   |       +---curve
    |   |   |       +---ec
    |   |   |       +---eddsa
    |   |   |       \---precomputed
    |   |   \---node_modules
    |   |       \---bn.js
    |   |           +---lib
    |   |           \---util
    |   +---emoji-regex
    |   |   \---es2015
    |   +---emojis-list
    |   +---encodeurl
    |   +---encoding
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   \---iconv-lite
    |   |   |       +---.github
    |   |   |       +---encodings
    |   |   |       |   \---tables
    |   |   |       \---lib
    |   |   \---test
    |   +---end-of-stream
    |   +---engine.io
    |   |   +---lib
    |   |   |   +---parser-v3
    |   |   |   \---transports
    |   |   \---node_modules
    |   |       +---cookie
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---engine.io-parser
    |   |   \---lib
    |   +---enhanced-resolve
    |   |   \---lib
    |   |       \---util
    |   +---ent
    |   |   +---examples
    |   |   \---test
    |   +---entities
    |   |   \---lib
    |   |       \---maps
    |   +---env-paths
    |   +---err-code
    |   |   \---test
    |   +---errno
    |   +---error-ex
    |   +---es-abstract
    |   |   +---2015
    |   |   +---2016
    |   |   +---2017
    |   |   +---2018
    |   |   +---2019
    |   |   +---2020
    |   |   +---5
    |   |   +---helpers
    |   |   +---operations
    |   |   \---test
    |   |       \---helpers
    |   +---es-to-primitive
    |   |   +---.github
    |   |   +---helpers
    |   |   \---test
    |   +---es5-ext
    |   |   +---.github
    |   |   +---array
    |   |   |   +---#
    |   |   |   |   +---@@iterator
    |   |   |   |   +---concat
    |   |   |   |   +---copy-within
    |   |   |   |   +---entries
    |   |   |   |   +---fill
    |   |   |   |   +---filter
    |   |   |   |   +---find
    |   |   |   |   +---find-index
    |   |   |   |   +---keys
    |   |   |   |   +---map
    |   |   |   |   +---slice
    |   |   |   |   +---splice
    |   |   |   |   \---values
    |   |   |   +---from
    |   |   |   \---of
    |   |   +---boolean
    |   |   +---date
    |   |   |   \---#
    |   |   +---error
    |   |   |   \---#
    |   |   +---function
    |   |   |   \---#
    |   |   +---iterable
    |   |   +---json
    |   |   +---math
    |   |   |   +---acosh
    |   |   |   +---asinh
    |   |   |   +---atanh
    |   |   |   +---cbrt
    |   |   |   +---clz32
    |   |   |   +---cosh
    |   |   |   +---expm1
    |   |   |   +---fround
    |   |   |   +---hypot
    |   |   |   +---imul
    |   |   |   +---log10
    |   |   |   +---log1p
    |   |   |   +---log2
    |   |   |   +---sign
    |   |   |   +---sinh
    |   |   |   +---tanh
    |   |   |   \---trunc
    |   |   +---number
    |   |   |   +---#
    |   |   |   +---epsilon
    |   |   |   +---is-finite
    |   |   |   +---is-integer
    |   |   |   +---is-nan
    |   |   |   +---is-safe-integer
    |   |   |   +---max-safe-integer
    |   |   |   \---min-safe-integer
    |   |   +---object
    |   |   |   +---assign
    |   |   |   +---entries
    |   |   |   +---keys
    |   |   |   \---set-prototype-of
    |   |   +---promise
    |   |   |   \---#
    |   |   |       \---finally
    |   |   +---reg-exp
    |   |   |   \---#
    |   |   |       +---match
    |   |   |       +---replace
    |   |   |       +---search
    |   |   |       +---split
    |   |   |       +---sticky
    |   |   |       \---unicode
    |   |   +---string
    |   |   |   +---#
    |   |   |   |   +---@@iterator
    |   |   |   |   +---code-point-at
    |   |   |   |   +---contains
    |   |   |   |   +---ends-with
    |   |   |   |   +---normalize
    |   |   |   |   +---repeat
    |   |   |   |   \---starts-with
    |   |   |   +---from-code-point
    |   |   |   \---raw
    |   |   \---test
    |   |       +---array
    |   |       |   +---#
    |   |       |   |   +---@@iterator
    |   |       |   |   +---concat
    |   |       |   |   +---copy-within
    |   |       |   |   +---entries
    |   |       |   |   +---fill
    |   |       |   |   +---filter
    |   |       |   |   +---find
    |   |       |   |   +---find-index
    |   |       |   |   +---keys
    |   |       |   |   +---map
    |   |       |   |   +---slice
    |   |       |   |   +---splice
    |   |       |   |   \---values
    |   |       |   +---from
    |   |       |   \---of
    |   |       +---boolean
    |   |       +---date
    |   |       |   \---#
    |   |       +---error
    |   |       |   \---#
    |   |       +---function
    |   |       |   \---#
    |   |       +---iterable
    |   |       +---json
    |   |       +---math
    |   |       |   +---acosh
    |   |       |   +---asinh
    |   |       |   +---atanh
    |   |       |   +---cbrt
    |   |       |   +---clz32
    |   |       |   +---cosh
    |   |       |   +---expm1
    |   |       |   +---fround
    |   |       |   +---hypot
    |   |       |   +---imul
    |   |       |   +---log10
    |   |       |   +---log1p
    |   |       |   +---log2
    |   |       |   +---sign
    |   |       |   +---sinh
    |   |       |   +---tanh
    |   |       |   \---trunc
    |   |       +---number
    |   |       |   +---#
    |   |       |   +---epsilon
    |   |       |   +---is-finite
    |   |       |   +---is-integer
    |   |       |   +---is-nan
    |   |       |   +---is-safe-integer
    |   |       |   +---max-safe-integer
    |   |       |   \---min-safe-integer
    |   |       +---object
    |   |       |   +---assign
    |   |       |   +---entries
    |   |       |   +---keys
    |   |       |   \---set-prototype-of
    |   |       +---promise
    |   |       |   \---#
    |   |       |       \---finally
    |   |       +---reg-exp
    |   |       |   \---#
    |   |       |       +---match
    |   |       |       +---replace
    |   |       |       +---search
    |   |       |       +---split
    |   |       |       +---sticky
    |   |       |       \---unicode
    |   |       \---string
    |   |           +---#
    |   |           |   +---@@iterator
    |   |           |   +---code-point-at
    |   |           |   +---contains
    |   |           |   +---ends-with
    |   |           |   +---normalize
    |   |           |   +---repeat
    |   |           |   \---starts-with
    |   |           +---from-code-point
    |   |           \---raw
    |   +---es6-iterator
    |   |   +---#
    |   |   \---test
    |   |       \---#
    |   +---es6-promise
    |   |   +---dist
    |   |   \---lib
    |   |       \---es6-promise
    |   |           \---promise
    |   +---es6-promisify
    |   |   \---dist
    |   +---es6-symbol
    |   |   +---.github
    |   |   +---lib
    |   |   |   \---private
    |   |   |       \---setup
    |   |   \---test
    |   +---escalade
    |   |   +---dist
    |   |   \---sync
    |   +---escape-html
    |   +---escape-string-regexp
    |   +---eslint-scope
    |   |   \---lib
    |   +---esprima
    |   |   +---bin
    |   |   \---dist
    |   +---esrecurse
    |   |   \---node_modules
    |   |       \---estraverse
    |   +---estraverse
    |   +---esutils
    |   |   \---lib
    |   +---etag
    |   +---eventemitter3
    |   |   \---umd
    |   +---events
    |   |   +---.github
    |   |   \---tests
    |   +---eventsource
    |   |   +---example
    |   |   \---lib
    |   +---evp_bytestokey
    |   +---execa
    |   |   \---lib
    |   +---exit
    |   |   +---lib
    |   |   \---test
    |   |       \---fixtures
    |   +---expand-brackets
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---define-property
    |   |       \---extend-shallow
    |   +---express
    |   |   +---lib
    |   |   |   +---middleware
    |   |   |   \---router
    |   |   \---node_modules
    |   |       +---array-flatten
    |   |       \---qs
    |   |           +---dist
    |   |           +---lib
    |   |           \---test
    |   +---ext
    |   |   +---docs
    |   |   |   +---function
    |   |   |   +---math
    |   |   |   +---object
    |   |   |   +---string
    |   |   |   +---string_
    |   |   |   \---thenable_
    |   |   +---function
    |   |   +---global-this
    |   |   +---lib
    |   |   |   \---private
    |   |   +---math
    |   |   +---node_modules
    |   |   |   \---type
    |   |   |       +---array
    |   |   |       +---array-length
    |   |   |       +---array-like
    |   |   |       +---date
    |   |   |       +---docs
    |   |   |       +---error
    |   |   |       +---finite
    |   |   |       +---function
    |   |   |       +---integer
    |   |   |       +---iterable
    |   |   |       +---lib
    |   |   |       |   \---ensure
    |   |   |       +---natural-number
    |   |   |       +---number
    |   |   |       +---object
    |   |   |       +---plain-function
    |   |   |       +---plain-object
    |   |   |       +---promise
    |   |   |       +---prototype
    |   |   |       +---reg-exp
    |   |   |       +---safe-integer
    |   |   |       +---string
    |   |   |       +---test
    |   |   |       |   +---array
    |   |   |       |   +---array-length
    |   |   |       |   +---array-like
    |   |   |       |   +---date
    |   |   |       |   +---error
    |   |   |       |   +---finite
    |   |   |       |   +---function
    |   |   |       |   +---integer
    |   |   |       |   +---iterable
    |   |   |       |   +---lib
    |   |   |       |   +---natural-number
    |   |   |       |   +---number
    |   |   |       |   +---object
    |   |   |       |   +---plain-function
    |   |   |       |   +---plain-object
    |   |   |       |   +---promise
    |   |   |       |   +---prototype
    |   |   |       |   +---reg-exp
    |   |   |       |   +---safe-integer
    |   |   |       |   +---string
    |   |   |       |   +---thenable
    |   |   |       |   +---time-value
    |   |   |       |   +---value
    |   |   |       |   \---_lib
    |   |   |       +---thenable
    |   |   |       +---time-value
    |   |   |       \---value
    |   |   +---object
    |   |   |   \---entries
    |   |   +---string
    |   |   +---string_
    |   |   |   \---includes
    |   |   +---test
    |   |   |   +---function
    |   |   |   +---global-this
    |   |   |   +---math
    |   |   |   +---object
    |   |   |   |   \---entries
    |   |   |   +---string
    |   |   |   +---string_
    |   |   |   |   \---includes
    |   |   |   \---thenable_
    |   |   \---thenable_
    |   +---extend
    |   +---extend-shallow
    |   |   \---node_modules
    |   |       \---is-extendable
    |   +---external-editor
    |   |   \---main
    |   |       \---errors
    |   +---extglob
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---define-property
    |   |       +---extend-shallow
    |   |       +---is-accessor-descriptor
    |   |       +---is-data-descriptor
    |   |       \---is-descriptor
    |   +---extsprintf
    |   |   \---lib
    |   +---fast-deep-equal
    |   +---fast-glob
    |   |   +---node_modules
    |   |   |   +---braces
    |   |   |   |   \---lib
    |   |   |   +---fill-range
    |   |   |   +---glob-parent
    |   |   |   +---is-number
    |   |   |   +---micromatch
    |   |   |   \---to-regex-range
    |   |   \---out
    |   |       +---managers
    |   |       +---providers
    |   |       |   +---filters
    |   |       |   +---matchers
    |   |       |   \---transformers
    |   |       +---readers
    |   |       +---types
    |   |       \---utils
    |   +---fast-json-stable-stringify
    |   |   +---benchmark
    |   |   +---example
    |   |   \---test
    |   +---fastparse
    |   |   \---lib
    |   +---fastq
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---faye-websocket
    |   |   \---lib
    |   |       \---faye
    |   |           \---websocket
    |   |               \---api
    |   +---figgy-pudding
    |   +---figures
    |   +---file-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       \---schema-utils
    |   |           +---declarations
    |   |           |   +---keywords
    |   |           |   \---util
    |   |           \---dist
    |   |               +---keywords
    |   |               \---util
    |   +---file-uri-to-path
    |   |   \---test
    |   +---fileset
    |   |   +---lib
    |   |   \---test
    |   |       \---fixtures
    |   +---fill-range
    |   |   \---node_modules
    |   |       \---extend-shallow
    |   +---finalhandler
    |   +---find-cache-dir
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---make-dir
    |   |       \---semver
    |   |           \---bin
    |   +---find-up
    |   +---flatted
    |   |   +---.github
    |   |   +---cjs
    |   |   \---esm
    |   +---flush-write-stream
    |   +---follow-redirects
    |   +---font-awesome
    |   |   +---css
    |   |   +---fonts
    |   |   +---less
    |   |   \---scss
    |   +---for-in
    |   +---forever-agent
    |   +---form-data
    |   |   \---lib
    |   +---forwarded
    |   +---fraction.js
    |   +---fragment-cache
    |   +---fresh
    |   +---from2
    |   +---fs-access
    |   +---fs-extra
    |   |   \---lib
    |   |       +---copy
    |   |       +---copy-sync
    |   |       +---empty
    |   |       +---ensure
    |   |       +---fs
    |   |       +---json
    |   |       +---mkdirs
    |   |       +---move
    |   |       +---move-sync
    |   |       +---output
    |   |       +---path-exists
    |   |       +---remove
    |   |       \---util
    |   +---fs-minipass
    |   +---fs-write-stream-atomic
    |   |   \---test
    |   +---fs.realpath
    |   +---fsevents
    |   |   \---src
    |   +---function-bind
    |   |   \---test
    |   +---gauge
    |   +---gensync
    |   |   \---test
    |   +---get-caller-file
    |   +---get-intrinsic
    |   |   +---.github
    |   |   \---test
    |   +---get-stream
    |   +---get-value
    |   +---getpass
    |   |   \---lib
    |   +---glob
    |   +---glob-parent
    |   |   \---node_modules
    |   |       \---is-glob
    |   +---globals
    |   +---globby
    |   |   \---node_modules
    |   |       \---array-union
    |   +---graceful-fs
    |   +---handle-thing
    |   |   +---lib
    |   |   \---test
    |   +---har-schema
    |   |   \---lib
    |   +---har-validator
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       \---fast-deep-equal
    |   |           \---es6
    |   +---has
    |   |   +---src
    |   |   \---test
    |   +---has-ansi
    |   +---has-flag
    |   +---has-symbols
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   |       \---shams
    |   +---has-unicode
    |   +---has-value
    |   +---has-values
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---hash-base
    |   |   \---node_modules
    |   |       +---readable-stream
    |   |       |   \---lib
    |   |       |       \---internal
    |   |       |           \---streams
    |   |       \---safe-buffer
    |   +---hash.js
    |   |   +---lib
    |   |   |   \---hash
    |   |   |       \---sha
    |   |   \---test
    |   +---hex-color-regex
    |   +---hmac-drbg
    |   |   +---lib
    |   |   \---test
    |   |       \---fixtures
    |   +---hosted-git-info
    |   |   \---node_modules
    |   |       +---lru-cache
    |   |       \---yallist
    |   +---hpack.js
    |   |   +---bin
    |   |   +---lib
    |   |   |   \---hpack
    |   |   +---test
    |   |   \---tools
    |   +---hsl-regex
    |   |   \---test
    |   +---hsla-regex
    |   |   \---test
    |   +---html-comment-regex
    |   +---html-entities
    |   |   \---lib
    |   +---html-escaper
    |   |   +---cjs
    |   |   +---esm
    |   |   \---test
    |   +---http-cache-semantics
    |   +---http-deceiver
    |   |   +---lib
    |   |   \---test
    |   +---http-errors
    |   |   \---node_modules
    |   |       \---inherits
    |   +---http-parser-js
    |   +---http-proxy
    |   |   \---lib
    |   |       \---http-proxy
    |   |           \---passes
    |   +---http-proxy-agent
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---http-proxy-middleware
    |   |   \---lib
    |   +---http-signature
    |   |   \---lib
    |   +---https-browserify
    |   +---https-proxy-agent
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---humanize-ms
    |   +---iconv-lite
    |   |   +---encodings
    |   |   |   \---tables
    |   |   \---lib
    |   +---icss-utils
    |   |   \---src
    |   +---ieee754
    |   +---iferr
    |   |   \---test
    |   +---ignore
    |   +---ignore-walk
    |   +---image-size
    |   |   +---bin
    |   |   \---lib
    |   |       \---types
    |   +---immediate
    |   |   +---dist
    |   |   \---lib
    |   +---import-fresh
    |   +---import-local
    |   |   +---fixtures
    |   |   \---node_modules
    |   |       \---pkg-dir
    |   +---imurmurhash
    |   +---indent-string
    |   +---indexes-of
    |   +---infer-owner
    |   +---inflight
    |   +---inherits
    |   +---ini
    |   +---inquirer
    |   |   +---lib
    |   |   |   +---objects
    |   |   |   +---prompts
    |   |   |   +---ui
    |   |   |   \---utils
    |   |   \---node_modules
    |   |       +---ansi-regex
    |   |       +---ansi-styles
    |   |       +---chalk
    |   |       |   \---source
    |   |       +---color-convert
    |   |       +---color-name
    |   |       +---emoji-regex
    |   |       |   \---es2015
    |   |       +---has-flag
    |   |       +---is-fullwidth-code-point
    |   |       +---rxjs
    |   |       |   +---add
    |   |       |   |   +---observable
    |   |       |   |   |   \---dom
    |   |       |   |   \---operator
    |   |       |   +---ajax
    |   |       |   +---bundles
    |   |       |   +---fetch
    |   |       |   +---internal
    |   |       |   |   +---observable
    |   |       |   |   |   \---dom
    |   |       |   |   +---operators
    |   |       |   |   +---scheduled
    |   |       |   |   +---scheduler
    |   |       |   |   +---symbol
    |   |       |   |   +---testing
    |   |       |   |   \---util
    |   |       |   +---internal-compatibility
    |   |       |   +---migrations
    |   |       |   |   \---update-6_0_0
    |   |       |   +---observable
    |   |       |   |   \---dom
    |   |       |   +---operator
    |   |       |   +---operators
    |   |       |   +---scheduler
    |   |       |   +---src
    |   |       |   |   +---add
    |   |       |   |   |   +---observable
    |   |       |   |   |   |   \---dom
    |   |       |   |   |   \---operator
    |   |       |   |   +---ajax
    |   |       |   |   +---fetch
    |   |       |   |   +---internal
    |   |       |   |   |   +---observable
    |   |       |   |   |   |   \---dom
    |   |       |   |   |   +---operators
    |   |       |   |   |   +---scheduled
    |   |       |   |   |   +---scheduler
    |   |       |   |   |   +---symbol
    |   |       |   |   |   +---testing
    |   |       |   |   |   \---util
    |   |       |   |   +---internal-compatibility
    |   |       |   |   +---observable
    |   |       |   |   |   \---dom
    |   |       |   |   +---operator
    |   |       |   |   +---operators
    |   |       |   |   +---scheduler
    |   |       |   |   +---symbol
    |   |       |   |   +---testing
    |   |       |   |   +---util
    |   |       |   |   \---webSocket
    |   |       |   +---symbol
    |   |       |   +---testing
    |   |       |   +---util
    |   |       |   +---webSocket
    |   |       |   +---_esm2015
    |   |       |   |   +---ajax
    |   |       |   |   +---fetch
    |   |       |   |   +---internal
    |   |       |   |   |   +---observable
    |   |       |   |   |   |   \---dom
    |   |       |   |   |   +---operators
    |   |       |   |   |   +---scheduled
    |   |       |   |   |   +---scheduler
    |   |       |   |   |   +---symbol
    |   |       |   |   |   +---testing
    |   |       |   |   |   \---util
    |   |       |   |   +---internal-compatibility
    |   |       |   |   +---operators
    |   |       |   |   +---testing
    |   |       |   |   \---webSocket
    |   |       |   \---_esm5
    |   |       |       +---ajax
    |   |       |       +---fetch
    |   |       |       +---internal
    |   |       |       |   +---observable
    |   |       |       |   |   \---dom
    |   |       |       |   +---operators
    |   |       |       |   +---scheduled
    |   |       |       |   +---scheduler
    |   |       |       |   +---symbol
    |   |       |       |   +---testing
    |   |       |       |   \---util
    |   |       |       +---internal-compatibility
    |   |       |       +---operators
    |   |       |       +---testing
    |   |       |       \---webSocket
    |   |       +---string-width
    |   |       +---strip-ansi
    |   |       \---supports-color
    |   +---internal-ip
    |   +---interpret
    |   +---ip
    |   |   +---lib
    |   |   \---test
    |   +---ip-regex
    |   +---ipaddr.js
    |   |   \---lib
    |   +---is-absolute-url
    |   +---is-accessor-descriptor
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---is-arguments
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-arrayish
    |   +---is-binary-path
    |   +---is-buffer
    |   |   \---test
    |   +---is-callable
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-color-stop
    |   |   +---lib
    |   |   +---test
    |   |   \---util
    |   +---is-core-module
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-data-descriptor
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---is-date-object
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-descriptor
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---is-directory
    |   +---is-docker
    |   +---is-extendable
    |   +---is-extglob
    |   +---is-fullwidth-code-point
    |   +---is-glob
    |   +---is-interactive
    |   +---is-lambda
    |   +---is-negative-zero
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-number
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---is-obj
    |   +---is-path-cwd
    |   +---is-path-in-cwd
    |   +---is-path-inside
    |   +---is-plain-object
    |   +---is-regex
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-resolvable
    |   +---is-stream
    |   +---is-svg
    |   +---is-symbol
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---is-typedarray
    |   +---is-what
    |   |   +---.github
    |   |   +---dist
    |   |   +---src
    |   |   +---test
    |   |   \---types
    |   +---is-windows
    |   +---is-wsl
    |   +---isarray
    |   +---isbinaryfile
    |   |   \---lib
    |   +---isexe
    |   |   \---test
    |   +---isobject
    |   +---isstream
    |   +---istanbul-api
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---istanbul-lib-coverage
    |   |       |   \---lib
    |   |       +---istanbul-lib-instrument
    |   |       |   \---dist
    |   |       \---semver
    |   |           \---bin
    |   +---istanbul-lib-coverage
    |   |   \---lib
    |   +---istanbul-lib-hook
    |   |   \---lib
    |   +---istanbul-lib-instrument
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---.bin
    |   |       \---semver
    |   |           \---bin
    |   +---istanbul-lib-report
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---istanbul-lib-coverage
    |   |           \---lib
    |   +---istanbul-lib-source-maps
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       +---istanbul-lib-coverage
    |   |       |   \---lib
    |   |       +---ms
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---istanbul-reports
    |   |   \---lib
    |   |       +---clover
    |   |       +---cobertura
    |   |       +---html
    |   |       |   \---assets
    |   |       |       \---vendor
    |   |       +---json
    |   |       +---json-summary
    |   |       +---lcov
    |   |       +---lcovonly
    |   |       +---none
    |   |       +---teamcity
    |   |       +---text
    |   |       +---text-lcov
    |   |       \---text-summary
    |   +---jasmine
    |   |   +---bin
    |   |   +---lib
    |   |   |   +---examples
    |   |   |   +---filters
    |   |   |   \---reporters
    |   |   +---node_modules
    |   |   |   \---jasmine-core
    |   |   |       +---.github
    |   |   |       +---images
    |   |   |       +---jasmine_core.egg-info
    |   |   |       \---lib
    |   |   |           +---console
    |   |   |           \---jasmine-core
    |   |   |               \---example
    |   |   |                   +---node_example
    |   |   |                   |   +---lib
    |   |   |                   |   |   \---jasmine_examples
    |   |   |                   |   \---spec
    |   |   |                   |       +---helpers
    |   |   |                   |       |   \---jasmine_examples
    |   |   |                   |       \---jasmine_examples
    |   |   |                   +---spec
    |   |   |                   \---src
    |   |   \---tasks
    |   +---jasmine-core
    |   |   +---.github
    |   |   +---images
    |   |   |   \---__pycache__
    |   |   +---jasmine_core.egg-info
    |   |   \---lib
    |   |       +---console
    |   |       \---jasmine-core
    |   |           \---example
    |   |               +---node_example
    |   |               |   +---lib
    |   |               |   |   \---jasmine_examples
    |   |               |   \---spec
    |   |               |       +---helpers
    |   |               |       |   \---jasmine_examples
    |   |               |       \---jasmine_examples
    |   |               +---spec
    |   |               \---src
    |   +---jasmine-spec-reporter
    |   |   +---.github
    |   |   +---built
    |   |   |   +---display
    |   |   |   |   \---processors
    |   |   |   \---processors
    |   |   |       \---processors
    |   |   +---docs
    |   |   \---examples
    |   |       +---node
    |   |       |   \---spec
    |   |       |       +---helpers
    |   |       |       \---support
    |   |       +---protractor
    |   |       |   \---spec
    |   |       \---typescript
    |   |           \---spec
    |   |               +---helpers
    |   |               \---support
    |   +---jasminewd2
    |   +---jest-worker
    |   |   +---build
    |   |   |   +---base
    |   |   |   \---workers
    |   |   \---node_modules
    |   |       +---has-flag
    |   |       \---supports-color
    |   +---js-tokens
    |   +---js-yaml
    |   |   +---bin
    |   |   +---dist
    |   |   \---lib
    |   |       \---js-yaml
    |   |           +---schema
    |   |           \---type
    |   |               \---js
    |   +---jsbn
    |   +---jsesc
    |   |   +---bin
    |   |   \---man
    |   +---json-parse-better-errors
    |   +---json-parse-even-better-errors
    |   +---json-schema
    |   |   +---draft-00
    |   |   +---draft-01
    |   |   +---draft-02
    |   |   +---draft-03
    |   |   |   \---examples
    |   |   +---draft-04
    |   |   +---lib
    |   |   \---test
    |   +---json-schema-traverse
    |   |   \---spec
    |   |       \---fixtures
    |   +---json-stringify-safe
    |   |   \---test
    |   +---json3
    |   |   \---lib
    |   +---json5
    |   |   +---dist
    |   |   \---lib
    |   +---jsonc-parser
    |   |   \---lib
    |   |       +---esm
    |   |       |   \---impl
    |   |       \---umd
    |   |           \---impl
    |   +---jsonfile
    |   +---jsonparse
    |   |   +---examples
    |   |   +---samplejson
    |   |   \---test
    |   +---jsprim
    |   |   \---lib
    |   +---jszip
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---generate
    |   |   |   +---nodejs
    |   |   |   +---reader
    |   |   |   \---stream
    |   |   \---vendor
    |   +---karma
    |   |   +---bin
    |   |   +---common
    |   |   +---context
    |   |   +---lib
    |   |   |   +---init
    |   |   |   +---launchers
    |   |   |   +---middleware
    |   |   |   +---reporters
    |   |   |   \---utils
    |   |   +---node_modules
    |   |   |   +---.bin
    |   |   |   +---ansi-regex
    |   |   |   +---ansi-styles
    |   |   |   +---anymatch
    |   |   |   +---binary-extensions
    |   |   |   +---braces
    |   |   |   |   \---lib
    |   |   |   +---chokidar
    |   |   |   |   +---lib
    |   |   |   |   \---types
    |   |   |   +---cliui
    |   |   |   |   \---build
    |   |   |   |       \---lib
    |   |   |   +---color-convert
    |   |   |   +---color-name
    |   |   |   +---colors
    |   |   |   |   +---examples
    |   |   |   |   +---lib
    |   |   |   |   |   +---custom
    |   |   |   |   |   +---maps
    |   |   |   |   |   \---system
    |   |   |   |   \---themes
    |   |   |   +---emoji-regex
    |   |   |   |   \---es2015
    |   |   |   +---fill-range
    |   |   |   +---fsevents
    |   |   |   +---glob
    |   |   |   +---glob-parent
    |   |   |   +---is-binary-path
    |   |   |   +---is-fullwidth-code-point
    |   |   |   +---is-number
    |   |   |   +---mime
    |   |   |   |   \---types
    |   |   |   +---normalize-path
    |   |   |   +---readdirp
    |   |   |   +---rimraf
    |   |   |   +---source-map
    |   |   |   |   +---dist
    |   |   |   |   \---lib
    |   |   |   +---string-width
    |   |   |   +---strip-ansi
    |   |   |   +---tmp
    |   |   |   |   \---lib
    |   |   |   +---to-regex-range
    |   |   |   +---wrap-ansi
    |   |   |   +---y18n
    |   |   |   |   \---build
    |   |   |   |       \---lib
    |   |   |   |           \---platform-shims
    |   |   |   +---yargs
    |   |   |   |   +---build
    |   |   |   |   |   \---lib
    |   |   |   |   |       +---typings
    |   |   |   |   |       \---utils
    |   |   |   |   +---helpers
    |   |   |   |   +---lib
    |   |   |   |   |   \---platform-shims
    |   |   |   |   \---locales
    |   |   |   \---yargs-parser
    |   |   |       \---build
    |   |   |           \---lib
    |   |   +---scripts
    |   |   \---static
    |   +---karma-chrome-launcher
    |   |   +---examples
    |   |   |   \---simple
    |   |   \---test
    |   +---karma-coverage-istanbul-reporter
    |   |   \---src
    |   +---karma-jasmine
    |   |   \---lib
    |   +---karma-jasmine-html-reporter
    |   |   +---screenshots
    |   |   \---src
    |   |       +---css
    |   |       \---lib
    |   +---karma-source-map-support
    |   |   \---lib
    |   +---killable
    |   +---kind-of
    |   +---klona
    |   |   +---dist
    |   |   +---full
    |   |   +---json
    |   |   \---lite
    |   +---less
    |   |   +---bin
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---less
    |   |   |   |   +---data
    |   |   |   |   +---environment
    |   |   |   |   +---functions
    |   |   |   |   +---parser
    |   |   |   |   +---tree
    |   |   |   |   \---visitors
    |   |   |   +---less-browser
    |   |   |   \---less-node
    |   |   +---node_modules
    |   |   |   \---source-map
    |   |   |       +---dist
    |   |   |       \---lib
    |   |   \---test
    |   |       +---browser
    |   |       |   +---css
    |   |       |   |   +---global-vars
    |   |       |   |   +---modify-vars
    |   |       |   |   +---plugin
    |   |       |   |   +---postProcessor
    |   |       |   |   +---relative-urls
    |   |       |   |   +---rewrite-urls
    |   |       |   |   +---rootpath
    |   |       |   |   +---rootpath-relative
    |   |       |   |   \---rootpath-rewrite-urls
    |   |       |   +---generator
    |   |       |   \---less
    |   |       |       +---console-errors
    |   |       |       +---errors
    |   |       |       +---global-vars
    |   |       |       +---imports
    |   |       |       +---modify-vars
    |   |       |       |   \---imports
    |   |       |       +---nested-gradient-with-svg-gradient
    |   |       |       +---plugin
    |   |       |       +---postProcessor
    |   |       |       +---relative-urls
    |   |       |       +---rewrite-urls
    |   |       |       +---rootpath
    |   |       |       +---rootpath-relative
    |   |       |       \---rootpath-rewrite-urls
    |   |       +---plugins
    |   |       |   +---filemanager
    |   |       |   +---postprocess
    |   |       |   +---preprocess
    |   |       |   \---visitor
    |   |       +---sourcemaps
    |   |       \---sourcemaps-disable-annotation
    |   +---less-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       \---schema-utils
    |   |           +---declarations
    |   |           |   +---keywords
    |   |           |   \---util
    |   |           \---dist
    |   |               +---keywords
    |   |               \---util
    |   +---license-webpack-plugin
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       \---webpack-sources
    |   |           \---lib
    |   +---lie
    |   |   +---dist
    |   |   \---lib
    |   +---lines-and-columns
    |   |   \---dist
    |   +---loader-runner
    |   |   \---lib
    |   +---loader-utils
    |   |   \---lib
    |   +---locate-path
    |   +---lodash
    |   |   \---fp
    |   +---lodash.debounce
    |   +---lodash.memoize
    |   +---lodash.uniq
    |   +---log-symbols
    |   |   \---node_modules
    |   |       +---ansi-styles
    |   |       +---chalk
    |   |       |   \---source
    |   |       +---color-convert
    |   |       +---color-name
    |   |       +---has-flag
    |   |       \---supports-color
    |   +---log4js
    |   |   +---lib
    |   |   |   \---appenders
    |   |   +---node_modules
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   \---ms
    |   |   \---types
    |   +---loglevel
    |   |   +---.github
    |   |   +---dist
    |   |   +---lib
    |   |   \---test
    |   |       \---vendor
    |   +---lru-cache
    |   +---magic-string
    |   |   \---dist
    |   +---make-dir
    |   +---make-error
    |   |   \---dist
    |   +---make-fetch-happen
    |   |   +---node_modules
    |   |   |   +---lru-cache
    |   |   |   +---ssri
    |   |   |   \---yallist
    |   |   \---utils
    |   +---map-age-cleaner
    |   |   \---dist
    |   +---map-cache
    |   +---map-visit
    |   +---md5.js
    |   +---mdn-data
    |   |   +---api
    |   |   +---css
    |   |   \---l10n
    |   +---media-typer
    |   +---mem
    |   +---memory-fs
    |   |   \---lib
    |   +---merge-descriptors
    |   +---merge-source-map
    |   |   \---node_modules
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---merge-stream
    |   +---merge2
    |   +---methods
    |   +---micromatch
    |   |   \---lib
    |   +---miller-rabin
    |   |   +---bin
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   \---bn.js
    |   |   |       +---lib
    |   |   |       \---util
    |   |   \---test
    |   +---mime
    |   |   \---src
    |   +---mime-db
    |   +---mime-types
    |   +---mimic-fn
    |   +---mini-css-extract-plugin
    |   |   +---dist
    |   |   |   \---hmr
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       \---webpack-sources
    |   |           \---lib
    |   +---minimalistic-assert
    |   +---minimalistic-crypto-utils
    |   |   +---lib
    |   |   \---test
    |   +---minimatch
    |   +---minimist
    |   |   +---example
    |   |   \---test
    |   +---minipass
    |   |   \---node_modules
    |   |       \---yallist
    |   +---minipass-collect
    |   |   \---node_modules
    |   |       +---minipass
    |   |       \---yallist
    |   +---minipass-fetch
    |   |   \---lib
    |   +---minipass-flush
    |   |   \---node_modules
    |   |       +---minipass
    |   |       \---yallist
    |   +---minipass-json-stream
    |   +---minipass-pipeline
    |   |   \---node_modules
    |   |       +---minipass
    |   |       \---yallist
    |   +---minipass-sized
    |   |   \---test
    |   +---minizlib
    |   |   \---node_modules
    |   |       \---yallist
    |   +---mississippi
    |   +---mixin-deep
    |   |   \---node_modules
    |   |       \---is-extendable
    |   +---mkdirp
    |   |   \---bin
    |   +---move-concurrently
    |   +---ms
    |   +---multicast-dns
    |   +---multicast-dns-service-types
    |   +---mute-stream
    |   +---nan
    |   |   +---doc
    |   |   \---tools
    |   +---nanoid
    |   |   +---async
    |   |   +---bin
    |   |   +---non-secure
    |   |   \---url-alphabet
    |   +---nanomatch
    |   |   \---lib
    |   +---needle
    |   |   +---.github
    |   |   |   \---workflows
    |   |   +---bin
    |   |   +---examples
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   \---ms
    |   |   \---test
    |   |       \---utils
    |   +---negotiator
    |   |   \---lib
    |   +---neo-async
    |   +---next-tick
    |   |   \---test
    |   +---nice-try
    |   |   \---src
    |   +---node-forge
    |   |   +---dist
    |   |   +---flash
    |   |   |   \---swf
    |   |   \---lib
    |   +---node-gyp
    |   |   +---.github
    |   |   |   \---workflows
    |   |   +---bin
    |   |   +---gyp
    |   |   |   +---.github
    |   |   |   |   \---workflows
    |   |   |   +---data
    |   |   |   |   \---win
    |   |   |   +---pylib
    |   |   |   |   \---gyp
    |   |   |   |       \---generator
    |   |   |   \---tools
    |   |   |       +---emacs
    |   |   |       |   \---testdata
    |   |   |       \---Xcode
    |   |   |           \---Specifications
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   +---.bin
    |   |   |   +---glob
    |   |   |   +---lru-cache
    |   |   |   +---rimraf
    |   |   |   +---semver
    |   |   |   |   +---bin
    |   |   |   |   +---classes
    |   |   |   |   +---functions
    |   |   |   |   +---internal
    |   |   |   |   \---ranges
    |   |   |   +---which
    |   |   |   |   \---bin
    |   |   |   \---yallist
    |   |   +---src
    |   |   \---test
    |   |       \---fixtures
    |   +---node-libs-browser
    |   |   +---mock
    |   |   \---node_modules
    |   |       +---buffer
    |   |       |   +---bin
    |   |       |   \---test
    |   |       |       \---node
    |   |       \---punycode
    |   +---node-releases
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---data
    |   |       +---processed
    |   |       +---raw
    |   |       \---release-schedule
    |   +---nopt
    |   |   +---bin
    |   |   \---lib
    |   +---normalize-path
    |   +---normalize-range
    |   +---normalize-url
    |   +---npm-bundled
    |   +---npm-install-checks
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---lru-cache
    |   |       +---semver
    |   |       |   +---bin
    |   |       |   +---classes
    |   |       |   +---functions
    |   |       |   +---internal
    |   |       |   \---ranges
    |   |       \---yallist
    |   +---npm-normalize-package-bin
    |   |   +---.github
    |   |   \---test
    |   +---npm-package-arg
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---lru-cache
    |   |       +---semver
    |   |       |   +---bin
    |   |       |   +---classes
    |   |       |   +---functions
    |   |       |   +---internal
    |   |       |   \---ranges
    |   |       \---yallist
    |   +---npm-packlist
    |   |   +---bin
    |   |   \---node_modules
    |   |       \---glob
    |   +---npm-pick-manifest
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---lru-cache
    |   |       +---semver
    |   |       |   +---bin
    |   |       |   +---classes
    |   |       |   +---functions
    |   |       |   +---internal
    |   |       |   \---ranges
    |   |       \---yallist
    |   +---npm-registry-fetch
    |   |   \---node_modules
    |   |       +---lru-cache
    |   |       \---yallist
    |   +---npm-run-path
    |   +---npmlog
    |   +---nth-check
    |   +---null-check
    |   +---number-is-nan
    |   +---oauth-sign
    |   +---object-assign
    |   +---object-copy
    |   |   \---node_modules
    |   |       +---define-property
    |   |       \---kind-of
    |   +---object-inspect
    |   |   +---.github
    |   |   |   \---workflows
    |   |   +---example
    |   |   \---test
    |   |       \---browser
    |   +---object-is
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---object-keys
    |   |   \---test
    |   +---object-visit
    |   +---object.assign
    |   |   +---.github
    |   |   |   \---workflows
    |   |   +---dist
    |   |   \---test
    |   +---object.getownpropertydescriptors
    |   |   +---.github
    |   |   |   \---workflows
    |   |   +---.nyc_output
    |   |   \---test
    |   +---object.pick
    |   +---object.values
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---obuf
    |   |   \---test
    |   +---on-finished
    |   +---on-headers
    |   +---once
    |   +---onetime
    |   +---open
    |   |   \---node_modules
    |   |       \---is-wsl
    |   +---opn
    |   +---ora
    |   |   \---node_modules
    |   |       +---ansi-regex
    |   |       +---ansi-styles
    |   |       +---chalk
    |   |       |   \---source
    |   |       +---cli-cursor
    |   |       +---color-convert
    |   |       +---color-name
    |   |       +---has-flag
    |   |       +---onetime
    |   |       +---restore-cursor
    |   |       +---strip-ansi
    |   |       \---supports-color
    |   +---original
    |   +---os-browserify
    |   +---os-tmpdir
    |   +---p-defer
    |   +---p-finally
    |   +---p-is-promise
    |   +---p-limit
    |   +---p-locate
    |   +---p-map
    |   +---p-retry
    |   |   \---node_modules
    |   |       \---retry
    |   |           +---example
    |   |           +---lib
    |   |           \---test
    |   |               \---integration
    |   +---p-try
    |   +---pacote
    |   |   +---lib
    |   |   |   \---util
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---chownr
    |   |       +---mkdirp
    |   |       |   +---bin
    |   |       |   \---lib
    |   |       +---rimraf
    |   |       \---ssri
    |   +---pako
    |   |   +---dist
    |   |   \---lib
    |   |       +---utils
    |   |       \---zlib
    |   +---parallel-transform
    |   +---parent-module
    |   |   \---node_modules
    |   |       \---callsites
    |   +---parse-asn1
    |   +---parse-json
    |   +---parse-node-version
    |   +---parse5
    |   |   \---lib
    |   |       +---common
    |   |       +---extensions
    |   |       |   +---error-reporting
    |   |       |   +---location-info
    |   |       |   \---position-tracking
    |   |       +---parser
    |   |       +---serializer
    |   |       +---tokenizer
    |   |       +---tree-adapters
    |   |       \---utils
    |   +---parse5-html-rewriting-stream
    |   |   \---lib
    |   +---parse5-htmlparser2-tree-adapter
    |   |   \---lib
    |   +---parse5-sax-parser
    |   |   \---lib
    |   +---parseurl
    |   +---pascalcase
    |   +---path-browserify
    |   |   \---test
    |   +---path-dirname
    |   +---path-exists
    |   +---path-is-absolute
    |   +---path-is-inside
    |   |   \---lib
    |   +---path-key
    |   +---path-parse
    |   +---path-to-regexp
    |   +---path-type
    |   +---pbkdf2
    |   |   \---lib
    |   +---performance-now
    |   |   +---lib
    |   |   +---src
    |   |   \---test
    |   |       \---scripts
    |   +---picomatch
    |   |   \---lib
    |   +---pify
    |   +---pinkie
    |   +---pinkie-promise
    |   +---pkg-dir
    |   |   \---node_modules
    |   |       +---find-up
    |   |       +---locate-path
    |   |       +---p-locate
    |   |       \---path-exists
    |   +---pnp-webpack-plugin
    |   |   \---fixtures
    |   +---portfinder
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---posix-character-classes
    |   +---postcss
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-calc
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-colormin
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-convert-values
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-discard-comments
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-discard-duplicates
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-discard-empty
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-discard-overridden
    |   |   +---dist
    |   |   +---node_modules
    |   |   |   +---postcss
    |   |   |   |   +---docs
    |   |   |   |   |   \---guidelines
    |   |   |   |   \---lib
    |   |   |   \---source-map
    |   |   |       +---dist
    |   |   |       \---lib
    |   |   \---src
    |   |       \---__tests__
    |   |           \---fixtures
    |   +---postcss-import
    |   |   \---lib
    |   +---postcss-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---cosmiconfig
    |   |       |   \---dist
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---import-fresh
    |   |       +---lru-cache
    |   |       +---parse-json
    |   |       +---resolve-from
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       +---semver
    |   |       |   +---bin
    |   |       |   +---classes
    |   |       |   +---functions
    |   |       |   +---internal
    |   |       |   \---ranges
    |   |       \---yallist
    |   +---postcss-merge-longhand
    |   |   +---dist
    |   |   |   \---lib
    |   |   |       \---decl
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-merge-rules
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-selector-parser
    |   |       |   \---dist
    |   |       |       \---selectors
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-minify-font-values
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-minify-gradients
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-minify-params
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-minify-selectors
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-selector-parser
    |   |       |   \---dist
    |   |       |       \---selectors
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-modules-extract-imports
    |   |   \---src
    |   +---postcss-modules-local-by-default
    |   |   \---src
    |   +---postcss-modules-scope
    |   |   \---src
    |   +---postcss-modules-values
    |   |   \---src
    |   +---postcss-normalize-charset
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-display-values
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-positions
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-repeat-style
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-string
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-timing-functions
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-unicode
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-url
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-normalize-whitespace
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-ordered-values
    |   |   +---dist
    |   |   |   +---lib
    |   |   |   \---rules
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-reduce-initial
    |   |   +---data
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-reduce-transforms
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-selector-parser
    |   |   \---dist
    |   |       +---selectors
    |   |       +---util
    |   |       \---__tests__
    |   |           \---util
    |   +---postcss-svgo
    |   |   +---dist
    |   |   |   \---lib
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-value-parser
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-unique-selectors
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---postcss-value-parser
    |   |   \---lib
    |   +---pretty-bytes
    |   +---primeicons
    |   |   +---fonts
    |   |   \---raw-svg
    |   +---primeng
    |   |   +---accordion
    |   |   +---api
    |   |   +---autocomplete
    |   |   +---avatar
    |   |   +---avatargroup
    |   |   +---badge
    |   |   +---blockui
    |   |   +---breadcrumb
    |   |   +---bundles
    |   |   +---button
    |   |   +---calendar
    |   |   +---captcha
    |   |   +---card
    |   |   +---carousel
    |   |   +---cascadeselect
    |   |   +---chart
    |   |   +---checkbox
    |   |   +---chip
    |   |   +---chips
    |   |   +---codehighlighter
    |   |   +---colorpicker
    |   |   +---confirmdialog
    |   |   +---confirmpopup
    |   |   +---contextmenu
    |   |   +---dataview
    |   |   +---defer
    |   |   +---dialog
    |   |   +---divider
    |   |   +---dom
    |   |   +---dragdrop
    |   |   +---dropdown
    |   |   +---dynamicdialog
    |   |   +---editor
    |   |   +---esm2015
    |   |   |   +---accordion
    |   |   |   +---api
    |   |   |   +---autocomplete
    |   |   |   +---avatar
    |   |   |   +---avatargroup
    |   |   |   +---badge
    |   |   |   +---blockui
    |   |   |   +---breadcrumb
    |   |   |   +---button
    |   |   |   +---calendar
    |   |   |   +---captcha
    |   |   |   +---card
    |   |   |   +---carousel
    |   |   |   +---cascadeselect
    |   |   |   +---chart
    |   |   |   +---checkbox
    |   |   |   +---chip
    |   |   |   +---chips
    |   |   |   +---codehighlighter
    |   |   |   +---colorpicker
    |   |   |   +---confirmdialog
    |   |   |   +---confirmpopup
    |   |   |   +---contextmenu
    |   |   |   +---dataview
    |   |   |   +---defer
    |   |   |   +---dialog
    |   |   |   +---divider
    |   |   |   +---dom
    |   |   |   +---dragdrop
    |   |   |   +---dropdown
    |   |   |   +---dynamicdialog
    |   |   |   +---editor
    |   |   |   +---fieldset
    |   |   |   +---fileupload
    |   |   |   +---focustrap
    |   |   |   +---fullcalendar
    |   |   |   +---galleria
    |   |   |   +---gmap
    |   |   |   +---inplace
    |   |   |   +---inputmask
    |   |   |   +---inputnumber
    |   |   |   +---inputswitch
    |   |   |   +---inputtext
    |   |   |   +---inputtextarea
    |   |   |   +---keyfilter
    |   |   |   +---knob
    |   |   |   +---lightbox
    |   |   |   +---listbox
    |   |   |   +---megamenu
    |   |   |   +---menu
    |   |   |   +---menubar
    |   |   |   +---message
    |   |   |   +---messages
    |   |   |   +---multiselect
    |   |   |   +---orderlist
    |   |   |   +---organizationchart
    |   |   |   +---overlaypanel
    |   |   |   +---paginator
    |   |   |   +---panel
    |   |   |   +---panelmenu
    |   |   |   +---password
    |   |   |   +---picklist
    |   |   |   +---progressbar
    |   |   |   +---progressspinner
    |   |   |   +---radiobutton
    |   |   |   +---rating
    |   |   |   +---ripple
    |   |   |   +---scrollpanel
    |   |   |   +---scrolltop
    |   |   |   +---selectbutton
    |   |   |   +---sidebar
    |   |   |   +---skeleton
    |   |   |   +---slidemenu
    |   |   |   +---slider
    |   |   |   +---spinner
    |   |   |   +---splitbutton
    |   |   |   +---splitter
    |   |   |   +---steps
    |   |   |   +---table
    |   |   |   +---tabmenu
    |   |   |   +---tabview
    |   |   |   +---tag
    |   |   |   +---terminal
    |   |   |   +---tieredmenu
    |   |   |   +---timeline
    |   |   |   +---toast
    |   |   |   +---togglebutton
    |   |   |   +---toolbar
    |   |   |   +---tooltip
    |   |   |   +---tree
    |   |   |   +---treetable
    |   |   |   +---tristatecheckbox
    |   |   |   +---utils
    |   |   |   \---virtualscroller
    |   |   +---fesm2015
    |   |   +---fieldset
    |   |   +---fileupload
    |   |   +---focustrap
    |   |   +---fullcalendar
    |   |   +---galleria
    |   |   +---gmap
    |   |   +---inplace
    |   |   +---inputmask
    |   |   +---inputnumber
    |   |   +---inputswitch
    |   |   +---inputtext
    |   |   +---inputtextarea
    |   |   +---keyfilter
    |   |   +---knob
    |   |   +---lightbox
    |   |   +---listbox
    |   |   +---megamenu
    |   |   +---menu
    |   |   +---menubar
    |   |   +---message
    |   |   +---messages
    |   |   +---multiselect
    |   |   +---node_modules
    |   |   |   \---tslib
    |   |   |       \---modules
    |   |   +---orderlist
    |   |   +---organizationchart
    |   |   +---overlaypanel
    |   |   +---paginator
    |   |   +---panel
    |   |   +---panelmenu
    |   |   +---password
    |   |   +---picklist
    |   |   +---progressbar
    |   |   +---progressspinner
    |   |   +---radiobutton
    |   |   +---rating
    |   |   +---resources
    |   |   |   +---components
    |   |   |   |   +---accordion
    |   |   |   |   +---autocomplete
    |   |   |   |   +---avatar
    |   |   |   |   +---avatargroup
    |   |   |   |   +---badge
    |   |   |   |   +---blockui
    |   |   |   |   +---breadcrumb
    |   |   |   |   +---button
    |   |   |   |   +---calendar
    |   |   |   |   +---card
    |   |   |   |   +---carousel
    |   |   |   |   +---cascadeselect
    |   |   |   |   +---checkbox
    |   |   |   |   +---chip
    |   |   |   |   +---chips
    |   |   |   |   +---colorpicker
    |   |   |   |   |   \---images
    |   |   |   |   +---common
    |   |   |   |   +---confirmpopup
    |   |   |   |   +---contextmenu
    |   |   |   |   +---dataview
    |   |   |   |   +---dialog
    |   |   |   |   +---divider
    |   |   |   |   +---dropdown
    |   |   |   |   +---editor
    |   |   |   |   +---fieldset
    |   |   |   |   +---fileupload
    |   |   |   |   +---galleria
    |   |   |   |   +---inplace
    |   |   |   |   +---inputnumber
    |   |   |   |   +---inputswitch
    |   |   |   |   +---inputtext
    |   |   |   |   +---inputtextarea
    |   |   |   |   +---knob
    |   |   |   |   +---lightbox
    |   |   |   |   |   \---images
    |   |   |   |   +---listbox
    |   |   |   |   +---megamenu
    |   |   |   |   +---menu
    |   |   |   |   +---menubar
    |   |   |   |   +---message
    |   |   |   |   +---messages
    |   |   |   |   +---multiselect
    |   |   |   |   +---orderlist
    |   |   |   |   +---organizationchart
    |   |   |   |   +---overlaypanel
    |   |   |   |   +---paginator
    |   |   |   |   +---panel
    |   |   |   |   +---panelmenu
    |   |   |   |   +---password
    |   |   |   |   |   \---images
    |   |   |   |   +---picklist
    |   |   |   |   +---progressbar
    |   |   |   |   +---progressspinner
    |   |   |   |   +---radiobutton
    |   |   |   |   +---rating
    |   |   |   |   +---ripple
    |   |   |   |   +---scrollpanel
    |   |   |   |   +---scrolltop
    |   |   |   |   +---sidebar
    |   |   |   |   +---skeleton
    |   |   |   |   +---slidemenu
    |   |   |   |   +---slider
    |   |   |   |   +---spinner
    |   |   |   |   +---splitbutton
    |   |   |   |   +---splitter
    |   |   |   |   +---steps
    |   |   |   |   +---table
    |   |   |   |   +---tabmenu
    |   |   |   |   +---tabview
    |   |   |   |   +---tag
    |   |   |   |   +---terminal
    |   |   |   |   +---tieredmenu
    |   |   |   |   +---timeline
    |   |   |   |   +---toast
    |   |   |   |   +---toolbar
    |   |   |   |   +---tooltip
    |   |   |   |   +---tree
    |   |   |   |   |   \---images
    |   |   |   |   +---treetable
    |   |   |   |   \---virtualscroller
    |   |   |   +---images
    |   |   |   \---themes
    |   |   |       +---arya-blue
    |   |   |       +---arya-green
    |   |   |       +---arya-orange
    |   |   |       +---arya-purple
    |   |   |       +---bootstrap4-dark-blue
    |   |   |       +---bootstrap4-dark-purple
    |   |   |       +---bootstrap4-light-blue
    |   |   |       +---bootstrap4-light-purple
    |   |   |       +---fluent-light
    |   |   |       +---luna-amber
    |   |   |       +---luna-blue
    |   |   |       +---luna-green
    |   |   |       +---luna-pink
    |   |   |       +---md-dark-deeppurple
    |   |   |       |   \---fonts
    |   |   |       +---md-dark-indigo
    |   |   |       |   \---fonts
    |   |   |       +---md-light-deeppurple
    |   |   |       |   \---fonts
    |   |   |       +---md-light-indigo
    |   |   |       |   \---fonts
    |   |   |       +---mdc-dark-deeppurple
    |   |   |       |   \---fonts
    |   |   |       +---mdc-dark-indigo
    |   |   |       |   \---fonts
    |   |   |       +---mdc-light-deeppurple
    |   |   |       |   \---fonts
    |   |   |       +---mdc-light-indigo
    |   |   |       |   \---fonts
    |   |   |       +---nova
    |   |   |       +---nova-accent
    |   |   |       +---nova-alt
    |   |   |       +---rhea
    |   |   |       +---saga-blue
    |   |   |       +---saga-green
    |   |   |       +---saga-orange
    |   |   |       +---saga-purple
    |   |   |       +---vela-blue
    |   |   |       +---vela-green
    |   |   |       +---vela-orange
    |   |   |       \---vela-purple
    |   |   +---ripple
    |   |   +---scrollpanel
    |   |   +---scrolltop
    |   |   +---selectbutton
    |   |   +---sidebar
    |   |   +---skeleton
    |   |   +---slidemenu
    |   |   +---slider
    |   |   +---spinner
    |   |   +---splitbutton
    |   |   +---splitter
    |   |   +---steps
    |   |   +---table
    |   |   +---tabmenu
    |   |   +---tabview
    |   |   +---tag
    |   |   +---terminal
    |   |   +---tieredmenu
    |   |   +---timeline
    |   |   +---toast
    |   |   +---togglebutton
    |   |   +---toolbar
    |   |   +---tooltip
    |   |   +---tree
    |   |   +---treetable
    |   |   +---tristatecheckbox
    |   |   +---utils
    |   |   \---virtualscroller
    |   +---process
    |   +---process-nextick-args
    |   +---promise-inflight
    |   +---promise-retry
    |   |   \---test
    |   +---protractor
    |   |   +---.circleci
    |   |   +---bin
    |   |   +---built
    |   |   |   +---debugger
    |   |   |   |   +---clients
    |   |   |   |   \---modes
    |   |   |   +---driverProviders
    |   |   |   +---frameworks
    |   |   |   +---selenium-webdriver
    |   |   |   \---webdriver-js-extender
    |   |   +---example
    |   |   |   \---angular_material
    |   |   +---exampleTypescript
    |   |   |   \---asyncAwait
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---ansi-regex
    |   |       +---ansi-styles
    |   |       +---camelcase
    |   |       +---chalk
    |   |       +---cliui
    |   |       |   \---node_modules
    |   |       |       \---strip-ansi
    |   |       +---color-convert
    |   |       +---color-name
    |   |       +---del
    |   |       +---emoji-regex
    |   |       |   \---es2015
    |   |       +---find-up
    |   |       +---globby
    |   |       +---ini
    |   |       +---is-fullwidth-code-point
    |   |       +---locate-path
    |   |       +---p-locate
    |   |       +---path-exists
    |   |       +---pify
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       +---source-map-support
    |   |       +---string-width
    |   |       |   \---node_modules
    |   |       |       \---strip-ansi
    |   |       +---supports-color
    |   |       +---webdriver-manager
    |   |       |   +---.circleci
    |   |       |   +---bin
    |   |       |   +---built
    |   |       |   |   \---lib
    |   |       |   |       +---binaries
    |   |       |   |       +---cli
    |   |       |   |       +---cmds
    |   |       |   |       \---files
    |   |       |   \---docs
    |   |       +---wrap-ansi
    |   |       |   \---node_modules
    |   |       |       +---ansi-styles
    |   |       |       \---strip-ansi
    |   |       +---yargs
    |   |       |   +---build
    |   |       |   |   \---lib
    |   |       |   \---locales
    |   |       \---yargs-parser
    |   |           \---lib
    |   +---proxy-addr
    |   +---prr
    |   +---psl
    |   |   +---data
    |   |   \---dist
    |   +---public-encrypt
    |   |   +---node_modules
    |   |   |   \---bn.js
    |   |   |       +---lib
    |   |   |       \---util
    |   |   \---test
    |   +---puka
    |   +---pump
    |   +---pumpify
    |   |   \---node_modules
    |   |       \---pump
    |   +---punycode
    |   +---q
    |   +---qjobs
    |   |   +---examples
    |   |   \---tests
    |   +---qs
    |   |   +---dist
    |   |   +---lib
    |   |   \---test
    |   +---querystring
    |   |   \---test
    |   +---querystring-es3
    |   |   \---test
    |   +---querystringify
    |   +---randombytes
    |   +---randomfill
    |   +---range-parser
    |   +---raw-body
    |   |   \---node_modules
    |   |       \---bytes
    |   +---raw-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       \---schema-utils
    |   |           +---declarations
    |   |           |   +---keywords
    |   |           |   \---util
    |   |           \---dist
    |   |               +---keywords
    |   |               \---util
    |   +---read-cache
    |   |   \---node_modules
    |   |       \---pify
    |   +---read-package-json-fast
    |   +---readable-stream
    |   |   +---doc
    |   |   |   \---wg-meetings
    |   |   \---lib
    |   |       \---internal
    |   |           \---streams
    |   +---readdir-scoped-modules
    |   +---readdirp
    |   +---rechoir
    |   |   \---lib
    |   +---reflect-metadata
    |   |   \---docs
    |   +---regenerate
    |   +---regenerate-unicode-properties
    |   |   +---Binary_Property
    |   |   +---General_Category
    |   |   +---Script
    |   |   \---Script_Extensions
    |   +---regenerator-runtime
    |   +---regenerator-transform
    |   |   +---lib
    |   |   \---src
    |   +---regex-not
    |   +---regex-parser
    |   |   \---lib
    |   |       \---typings
    |   +---regexp.prototype.flags
    |   |   \---test
    |   +---regexpu-core
    |   |   \---data
    |   +---regjsgen
    |   +---regjsparser
    |   |   \---bin
    |   +---remove-trailing-separator
    |   +---repeat-element
    |   +---repeat-string
    |   +---request
    |   |   \---lib
    |   +---require-directory
    |   +---require-main-filename
    |   +---requires-port
    |   +---resolve
    |   |   +---.github
    |   |   |   \---workflows
    |   |   +---example
    |   |   +---lib
    |   |   \---test
    |   |       +---dotdot
    |   |       |   \---abc
    |   |       +---module_dir
    |   |       |   +---xmodules
    |   |       |   |   \---aaa
    |   |       |   +---ymodules
    |   |       |   |   \---aaa
    |   |       |   \---zmodules
    |   |       |       \---bbb
    |   |       +---node_path
    |   |       |   +---x
    |   |       |   |   +---aaa
    |   |       |   |   \---ccc
    |   |       |   \---y
    |   |       |       +---bbb
    |   |       |       \---ccc
    |   |       +---pathfilter
    |   |       |   \---deep_ref
    |   |       +---precedence
    |   |       |   +---aaa
    |   |       |   \---bbb
    |   |       +---resolver
    |   |       |   +---baz
    |   |       |   +---browser_field
    |   |       |   +---dot_main
    |   |       |   +---dot_slash_main
    |   |       |   +---incorrect_main
    |   |       |   +---invalid_main
    |   |       |   +---multirepo
    |   |       |   |   \---packages
    |   |       |   |       +---package-a
    |   |       |   |       \---package-b
    |   |       |   +---nested_symlinks
    |   |       |   |   \---mylib
    |   |       |   +---other_path
    |   |       |   |   \---lib
    |   |       |   +---quux
    |   |       |   |   \---foo
    |   |       |   +---same_names
    |   |       |   |   \---foo
    |   |       |   +---symlinked
    |   |       |   |   +---package
    |   |       |   |   \---_
    |   |       |   |       +---node_modules
    |   |       |   |       \---symlink_target
    |   |       |   \---without_basedir
    |   |       \---shadowed_core
    |   |           \---node_modules
    |   |               \---util
    |   +---resolve-cwd
    |   +---resolve-from
    |   +---resolve-url
    |   |   \---test
    |   +---resolve-url-loader
    |   |   +---lib
    |   |   |   \---engine
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---camelcase
    |   |       +---emojis-list
    |   |       +---json5
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       +---loader-utils
    |   |       |   \---lib
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   +---api
    |   |       |   |   |   \---assets
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---restore-cursor
    |   +---ret
    |   |   \---lib
    |   +---retry
    |   |   +---example
    |   |   +---lib
    |   |   \---test
    |   |       \---integration
    |   +---reusify
    |   |   \---benchmarks
    |   +---rework
    |   |   \---node_modules
    |   |       +---convert-source-map
    |   |       |   +---example
    |   |       |   \---test
    |   |       |       \---fixtures
    |   |       +---css
    |   |       |   \---lib
    |   |       |       +---parse
    |   |       |       \---stringify
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---rework-visit
    |   +---rfdc
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---rgb-regex
    |   |   \---test
    |   +---rgba-regex
    |   |   \---test
    |   +---rimraf
    |   +---ripemd160
    |   +---rollup
    |   |   +---dist
    |   |   |   +---bin
    |   |   |   +---es
    |   |   |   |   \---shared
    |   |   |   \---shared
    |   |   \---node_modules
    |   |       \---fsevents
    |   +---run-async
    |   +---run-parallel
    |   +---run-queue
    |   +---rxjs
    |   |   +---add
    |   |   |   +---observable
    |   |   |   |   \---dom
    |   |   |   \---operator
    |   |   +---ajax
    |   |   +---bundles
    |   |   +---internal
    |   |   |   +---observable
    |   |   |   |   \---dom
    |   |   |   +---operators
    |   |   |   +---scheduler
    |   |   |   +---symbol
    |   |   |   +---testing
    |   |   |   \---util
    |   |   +---internal-compatibility
    |   |   +---migrations
    |   |   |   \---update-6_0_0
    |   |   +---observable
    |   |   |   \---dom
    |   |   +---operator
    |   |   +---operators
    |   |   +---scheduler
    |   |   +---src
    |   |   |   +---add
    |   |   |   |   +---observable
    |   |   |   |   |   \---dom
    |   |   |   |   \---operator
    |   |   |   +---ajax
    |   |   |   +---internal
    |   |   |   |   +---observable
    |   |   |   |   |   \---dom
    |   |   |   |   +---operators
    |   |   |   |   +---scheduler
    |   |   |   |   +---symbol
    |   |   |   |   +---testing
    |   |   |   |   \---util
    |   |   |   +---internal-compatibility
    |   |   |   +---observable
    |   |   |   |   \---dom
    |   |   |   +---operator
    |   |   |   +---operators
    |   |   |   +---scheduler
    |   |   |   +---symbol
    |   |   |   +---testing
    |   |   |   +---util
    |   |   |   \---webSocket
    |   |   +---symbol
    |   |   +---testing
    |   |   +---util
    |   |   +---webSocket
    |   |   +---_esm2015
    |   |   |   +---ajax
    |   |   |   +---internal
    |   |   |   |   +---observable
    |   |   |   |   |   \---dom
    |   |   |   |   +---operators
    |   |   |   |   +---scheduler
    |   |   |   |   +---symbol
    |   |   |   |   +---testing
    |   |   |   |   \---util
    |   |   |   +---internal-compatibility
    |   |   |   +---operators
    |   |   |   +---testing
    |   |   |   \---webSocket
    |   |   \---_esm5
    |   |       +---ajax
    |   |       +---internal
    |   |       |   +---observable
    |   |       |   |   \---dom
    |   |       |   +---operators
    |   |       |   +---scheduler
    |   |       |   +---symbol
    |   |       |   +---testing
    |   |       |   \---util
    |   |       +---internal-compatibility
    |   |       +---operators
    |   |       +---testing
    |   |       \---webSocket
    |   +---safe-buffer
    |   +---safe-regex
    |   |   +---example
    |   |   \---test
    |   +---safer-buffer
    |   +---sass
    |   +---sass-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---lru-cache
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       +---semver
    |   |       |   +---bin
    |   |       |   +---classes
    |   |       |   +---functions
    |   |       |   +---internal
    |   |       |   \---ranges
    |   |       \---yallist
    |   +---saucelabs
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   +---agent-base
    |   |   |   |   \---test
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   +---https-proxy-agent
    |   |   |   \---ms
    |   |   \---test
    |   |       +---bootstrap
    |   |       \---helpers
    |   +---sax
    |   |   \---lib
    |   +---schema-utils
    |   |   +---declarations
    |   |   |   +---keywords
    |   |   |   \---util
    |   |   +---dist
    |   |   |   +---keywords
    |   |   |   \---util
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       \---fast-deep-equal
    |   |           \---es6
    |   +---select-hose
    |   |   +---lib
    |   |   \---test
    |   +---selenium-webdriver
    |   |   +---example
    |   |   +---firefox
    |   |   +---http
    |   |   +---io
    |   |   +---lib
    |   |   |   +---atoms
    |   |   |   \---test
    |   |   |       \---data
    |   |   |           +---child
    |   |   |           |   \---grandchild
    |   |   |           +---click_tests
    |   |   |           +---coordinates_tests
    |   |   |           +---css
    |   |   |           |   \---ui-lightness
    |   |   |           |       \---images
    |   |   |           +---firefox
    |   |   |           +---frame_switching_tests
    |   |   |           +---html5
    |   |   |           +---js
    |   |   |           |   +---skins
    |   |   |           |   |   \---lightgray
    |   |   |           |   |       +---fonts
    |   |   |           |   |       \---img
    |   |   |           |   \---themes
    |   |   |           |       \---modern
    |   |   |           +---key_tests
    |   |   |           +---locators_tests
    |   |   |           +---modal_dialogs
    |   |   |           +---overflow
    |   |   |           +---proxy
    |   |   |           +---safari
    |   |   |           +---screen
    |   |   |           +---scrolling_tests
    |   |   |           \---window_switching_tests
    |   |   +---net
    |   |   +---node_modules
    |   |   |   \---tmp
    |   |   |       +---coverage
    |   |   |       |   \---lcov-report
    |   |   |       |       \---lib
    |   |   |       +---lib
    |   |   |       \---test
    |   |   |           \---symlinkme
    |   |   +---remote
    |   |   +---test
    |   |   |   +---chrome
    |   |   |   +---firefox
    |   |   |   +---http
    |   |   |   +---io
    |   |   |   +---lib
    |   |   |   +---net
    |   |   |   +---phantomjs
    |   |   |   \---testing
    |   |   \---testing
    |   +---selfsigned
    |   |   \---test
    |   +---semver
    |   |   \---bin
    |   +---semver-dsl
    |   +---semver-intersect
    |   +---send
    |   |   \---node_modules
    |   |       \---ms
    |   +---serialize-javascript
    |   +---serve-index
    |   |   +---node_modules
    |   |   |   +---http-errors
    |   |   |   +---inherits
    |   |   |   \---setprototypeof
    |   |   \---public
    |   |       \---icons
    |   +---serve-static
    |   +---set-blocking
    |   +---set-immediate-shim
    |   +---set-value
    |   |   \---node_modules
    |   |       \---extend-shallow
    |   +---setimmediate
    |   +---setprototypeof
    |   |   \---test
    |   +---sha.js
    |   |   \---test
    |   +---shallow-clone
    |   +---shebang-command
    |   +---shebang-regex
    |   +---shelljs
    |   |   +---bin
    |   |   \---src
    |   +---signal-exit
    |   +---simple-swizzle
    |   |   \---node_modules
    |   |       \---is-arrayish
    |   +---slash
    |   +---smart-buffer
    |   |   +---build
    |   |   +---docs
    |   |   \---typings
    |   +---snapdragon
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---define-property
    |   |       +---extend-shallow
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---snapdragon-node
    |   |   \---node_modules
    |   |       +---define-property
    |   |       +---is-accessor-descriptor
    |   |       +---is-data-descriptor
    |   |       \---is-descriptor
    |   +---snapdragon-util
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---socket.io
    |   |   +---client-dist
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---@types
    |   |       |   \---node
    |   |       |       +---fs
    |   |       |       +---ts3.4
    |   |       |       \---ts3.6
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---socket.io-adapter
    |   |   \---dist
    |   +---socket.io-parser
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---sockjs
    |   |   \---lib
    |   +---sockjs-client
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---event
    |   |   |   +---transport
    |   |   |   |   +---browser
    |   |   |   |   +---driver
    |   |   |   |   +---lib
    |   |   |   |   +---receiver
    |   |   |   |   \---sender
    |   |   |   \---utils
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---socks
    |   |   +---build
    |   |   |   +---client
    |   |   |   \---common
    |   |   +---docs
    |   |   |   \---examples
    |   |   |       +---javascript
    |   |   |       \---typescript
    |   |   \---typings
    |   |       +---client
    |   |       \---common
    |   +---socks-proxy-agent
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       \---ms
    |   +---source-list-map
    |   |   \---lib
    |   +---source-map
    |   |   +---dist
    |   |   \---lib
    |   +---source-map-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---iconv-lite
    |   |       |   +---.github
    |   |       |   +---encodings
    |   |       |   |   \---tables
    |   |       |   \---lib
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---source-map-resolve
    |   |   \---lib
    |   +---source-map-support
    |   |   \---node_modules
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---source-map-url
    |   |   \---test
    |   +---sourcemap-codec
    |   |   \---dist
    |   |       \---types
    |   +---spdy
    |   |   +---lib
    |   |   |   \---spdy
    |   |   +---node_modules
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   \---ms
    |   |   \---test
    |   +---spdy-transport
    |   |   +---lib
    |   |   |   \---spdy-transport
    |   |   |       \---protocol
    |   |   |           +---base
    |   |   |           +---http2
    |   |   |           \---spdy
    |   |   \---node_modules
    |   |       +---debug
    |   |       |   \---src
    |   |       +---ms
    |   |       \---readable-stream
    |   |           \---lib
    |   |               \---internal
    |   |                   \---streams
    |   +---speed-measure-webpack-plugin
    |   |   \---WrappedPlugin
    |   +---split-string
    |   +---sprintf-js
    |   |   +---demo
    |   |   +---dist
    |   |   +---src
    |   |   \---test
    |   +---sshpk
    |   |   +---bin
    |   |   +---lib
    |   |   |   \---formats
    |   |   \---man
    |   |       \---man1
    |   +---ssri
    |   +---stable
    |   +---static-extend
    |   |   \---node_modules
    |   |       \---define-property
    |   +---statuses
    |   +---stream-browserify
    |   |   \---test
    |   +---stream-each
    |   +---stream-http
    |   |   +---lib
    |   |   \---test
    |   |       +---browser
    |   |       |   \---lib
    |   |       +---node
    |   |       \---server
    |   |           \---static
    |   +---stream-shift
    |   +---streamroller
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   +---date-format
    |   |   |   |   +---lib
    |   |   |   |   \---test
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   \---ms
    |   |   \---test
    |   +---string-width
    |   +---string.prototype.trimend
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---string.prototype.trimstart
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---test
    |   +---string_decoder
    |   |   \---lib
    |   +---strip-ansi
    |   +---strip-eof
    |   +---style-loader
    |   |   +---dist
    |   |   |   \---runtime
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       \---schema-utils
    |   |           +---declarations
    |   |           |   +---keywords
    |   |           |   \---util
    |   |           \---dist
    |   |               +---keywords
    |   |               \---util
    |   +---stylehacks
    |   |   +---dist
    |   |   |   +---dictionary
    |   |   |   \---plugins
    |   |   \---node_modules
    |   |       +---postcss
    |   |       |   +---docs
    |   |       |   |   \---guidelines
    |   |       |   \---lib
    |   |       +---postcss-selector-parser
    |   |       |   \---dist
    |   |       |       \---selectors
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---stylus
    |   |   +---bin
    |   |   +---lib
    |   |   |   +---cache
    |   |   |   +---convert
    |   |   |   +---functions
    |   |   |   +---nodes
    |   |   |   +---stack
    |   |   |   \---visitor
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---debug
    |   |       |   \---src
    |   |       +---glob
    |   |       +---mkdirp
    |   |       |   +---bin
    |   |       |   \---lib
    |   |       \---semver
    |   |           \---bin
    |   +---stylus-loader
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---normalize-path
    |   |       \---schema-utils
    |   |           +---declarations
    |   |           |   +---keywords
    |   |           |   \---util
    |   |           \---dist
    |   |               +---keywords
    |   |               \---util
    |   +---supports-color
    |   +---svgo
    |   |   +---bin
    |   |   +---lib
    |   |   |   \---svgo
    |   |   \---plugins
    |   +---symbol-observable
    |   |   +---es
    |   |   \---lib
    |   +---tapable
    |   |   \---lib
    |   +---tar
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---.bin
    |   |       +---chownr
    |   |       +---fs-minipass
    |   |       +---minipass
    |   |       +---minizlib
    |   |       +---mkdirp
    |   |       |   +---bin
    |   |       |   \---lib
    |   |       \---yallist
    |   +---terser
    |   |   +---bin
    |   |   +---dist
    |   |   +---lib
    |   |   |   +---compress
    |   |   |   \---utils
    |   |   +---node_modules
    |   |   |   \---source-map-support
    |   |   |       \---node_modules
    |   |   |           \---source-map
    |   |   |               +---dist
    |   |   |               \---lib
    |   |   \---tools
    |   +---terser-webpack-plugin
    |   |   +---dist
    |   |   \---node_modules
    |   |       +---ajv
    |   |       |   +---dist
    |   |       |   +---lib
    |   |       |   |   +---compile
    |   |       |   |   +---dot
    |   |       |   |   +---dotjs
    |   |       |   |   \---refs
    |   |       |   \---scripts
    |   |       +---fast-deep-equal
    |   |       |   \---es6
    |   |       +---p-limit
    |   |       +---schema-utils
    |   |       |   +---declarations
    |   |       |   |   +---keywords
    |   |       |   |   \---util
    |   |       |   \---dist
    |   |       |       +---keywords
    |   |       |       \---util
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       \---webpack-sources
    |   |           \---lib
    |   +---text-table
    |   |   +---example
    |   |   \---test
    |   +---through
    |   |   \---test
    |   +---through2
    |   +---thunky
    |   +---timers-browserify
    |   +---timsort
    |   |   +---build
    |   |   \---src
    |   +---tmp
    |   |   \---lib
    |   +---to-arraybuffer
    |   +---to-object-path
    |   |   \---node_modules
    |   |       \---kind-of
    |   +---to-regex
    |   +---to-regex-range
    |   +---toidentifier
    |   +---tough-cookie
    |   |   \---lib
    |   +---tree-kill
    |   +---ts-node
    |   |   +---dist
    |   |   \---register
    |   +---ts-pnp
    |   +---tslib
    |   |   +---modules
    |   |   \---test
    |   |       \---validateModuleExportsMatchCommonJS
    |   +---tslint
    |   |   +---bin
    |   |   \---lib
    |   |       +---configs
    |   |       +---formatters
    |   |       +---language
    |   |       |   +---formatter
    |   |       |   +---rule
    |   |       |   \---walker
    |   |       +---rules
    |   |       |   +---code-examples
    |   |       |   \---completed-docs
    |   |       \---verify
    |   +---tsutils
    |   |   +---typeguard
    |   |   |   +---2.8
    |   |   |   +---2.9
    |   |   |   +---3.0
    |   |   |   \---next
    |   |   \---util
    |   +---tty-browserify
    |   +---tunnel-agent
    |   +---tweetnacl
    |   +---type
    |   |   +---array
    |   |   +---array-length
    |   |   +---array-like
    |   |   +---date
    |   |   +---error
    |   |   +---finite
    |   |   +---function
    |   |   +---integer
    |   |   +---iterable
    |   |   +---lib
    |   |   +---natural-number
    |   |   +---number
    |   |   +---object
    |   |   +---plain-function
    |   |   +---plain-object
    |   |   +---promise
    |   |   +---prototype
    |   |   +---reg-exp
    |   |   +---safe-integer
    |   |   +---string
    |   |   +---test
    |   |   |   +---array
    |   |   |   +---array-length
    |   |   |   +---array-like
    |   |   |   +---date
    |   |   |   +---error
    |   |   |   +---finite
    |   |   |   +---function
    |   |   |   +---integer
    |   |   |   +---iterable
    |   |   |   +---lib
    |   |   |   +---natural-number
    |   |   |   +---number
    |   |   |   +---object
    |   |   |   +---plain-function
    |   |   |   +---plain-object
    |   |   |   +---promise
    |   |   |   +---prototype
    |   |   |   +---reg-exp
    |   |   |   +---safe-integer
    |   |   |   +---string
    |   |   |   +---thenable
    |   |   |   +---time-value
    |   |   |   +---value
    |   |   |   \---_lib
    |   |   +---thenable
    |   |   +---time-value
    |   |   \---value
    |   +---type-fest
    |   |   \---source
    |   +---type-is
    |   +---typedarray
    |   |   +---example
    |   |   \---test
    |   |       \---server
    |   +---typescript
    |   |   +---bin
    |   |   \---lib
    |   |       +---cs
    |   |       +---de
    |   |       +---es
    |   |       +---fr
    |   |       +---it
    |   |       +---ja
    |   |       +---ko
    |   |       +---pl
    |   |       +---pt-br
    |   |       +---ru
    |   |       +---tr
    |   |       +---zh-cn
    |   |       \---zh-tw
    |   +---ua-parser-js
    |   |   +---dist
    |   |   +---src
    |   |   \---test
    |   +---unicode-canonical-property-names-ecmascript
    |   +---unicode-match-property-ecmascript
    |   +---unicode-match-property-value-ecmascript
    |   |   \---data
    |   +---unicode-property-aliases-ecmascript
    |   +---union-value
    |   +---uniq
    |   |   \---test
    |   +---uniqs
    |   +---unique-filename
    |   |   +---.nyc_output
    |   |   +---coverage
    |   |   |   \---__root__
    |   |   \---test
    |   +---unique-slug
    |   |   \---test
    |   +---universal-analytics
    |   |   +---lib
    |   |   +---node_modules
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   \---ms
    |   |   \---test
    |   +---universalify
    |   +---unpipe
    |   +---unquote
    |   +---unset-value
    |   |   \---node_modules
    |   |       +---has-value
    |   |       |   \---node_modules
    |   |       |       \---isobject
    |   |       \---has-values
    |   +---upath
    |   |   \---build
    |   |       \---code
    |   +---uri-js
    |   |   \---dist
    |   |       +---es5
    |   |       \---esnext
    |   |           \---schemes
    |   +---urix
    |   |   \---test
    |   +---url
    |   |   \---node_modules
    |   |       \---punycode
    |   +---url-parse
    |   |   \---dist
    |   +---use
    |   +---util
    |   |   +---node_modules
    |   |   |   \---inherits
    |   |   \---support
    |   +---util-deprecate
    |   +---util.promisify
    |   |   +---.github
    |   |   |   \---workflows
    |   |   \---node_modules
    |   |       \---es-abstract
    |   |           +---.github
    |   |           |   \---workflows
    |   |           +---2015
    |   |           +---2016
    |   |           +---2017
    |   |           +---2018
    |   |           +---2019
    |   |           +---5
    |   |           +---helpers
    |   |           +---operations
    |   |           \---test
    |   |               \---helpers
    |   +---utils-merge
    |   +---uuid
    |   |   +---bin
    |   |   \---lib
    |   +---validate-npm-package-name
    |   |   +---.nyc_output
    |   |   \---test
    |   +---vary
    |   +---vendors
    |   +---verror
    |   |   \---lib
    |   +---vm-browserify
    |   |   +---.github
    |   |   +---example
    |   |   |   \---run
    |   |   \---test
    |   +---void-elements
    |   |   \---test
    |   +---watchpack
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---anymatch
    |   |       +---binary-extensions
    |   |       +---braces
    |   |       |   \---lib
    |   |       +---chokidar
    |   |       |   +---lib
    |   |       |   \---types
    |   |       +---fill-range
    |   |       +---fsevents
    |   |       +---glob-parent
    |   |       +---is-binary-path
    |   |       +---is-number
    |   |       +---normalize-path
    |   |       +---readdirp
    |   |       \---to-regex-range
    |   +---watchpack-chokidar2
    |   |   \---node_modules
    |   |       +---chokidar
    |   |       |   +---lib
    |   |       |   \---types
    |   |       \---normalize-path
    |   +---wbuf
    |   |   \---test
    |   +---wcwidth
    |   |   +---docs
    |   |   \---test
    |   +---webdriver-js-extender
    |   |   \---built
    |   |       +---built
    |   |       |   +---built
    |   |       |   |   +---built
    |   |       |   |   |   +---built
    |   |       |   |   |   |   +---built
    |   |       |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   |   +---built
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   |   \---spec
    |   |       |   |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   |   \---spec
    |   |       |   |   |   |   |       \---command_tests
    |   |       |   |   |   |   \---spec
    |   |       |   |   |   |       \---command_tests
    |   |       |   |   |   \---spec
    |   |       |   |   |       \---command_tests
    |   |       |   |   \---spec
    |   |       |   |       \---command_tests
    |   |       |   \---spec
    |   |       |       \---command_tests
    |   |       +---lib
    |   |       \---spec
    |   |           +---command_tests
    |   |           \---mock-server
    |   |               \---commands
    |   +---webpack
    |   |   +---bin
    |   |   +---buildin
    |   |   +---declarations
    |   |   |   \---plugins
    |   |   |       +---debug
    |   |   |       \---optimize
    |   |   +---hot
    |   |   +---lib
    |   |   |   +---debug
    |   |   |   +---dependencies
    |   |   |   +---logging
    |   |   |   +---node
    |   |   |   +---optimize
    |   |   |   +---performance
    |   |   |   +---util
    |   |   |   +---wasm
    |   |   |   +---web
    |   |   |   \---webworker
    |   |   +---node_modules
    |   |   |   +---.bin
    |   |   |   +---ajv
    |   |   |   |   +---dist
    |   |   |   |   +---lib
    |   |   |   |   |   +---compile
    |   |   |   |   |   +---dot
    |   |   |   |   |   +---dotjs
    |   |   |   |   |   \---refs
    |   |   |   |   \---scripts
    |   |   |   +---cacache
    |   |   |   |   +---lib
    |   |   |   |   |   +---content
    |   |   |   |   |   \---util
    |   |   |   |   \---locales
    |   |   |   +---enhanced-resolve
    |   |   |   |   +---lib
    |   |   |   |   \---node_modules
    |   |   |   |       \---memory-fs
    |   |   |   |           \---lib
    |   |   |   +---fast-deep-equal
    |   |   |   |   \---es6
    |   |   |   +---find-cache-dir
    |   |   |   +---glob
    |   |   |   +---json5
    |   |   |   |   +---dist
    |   |   |   |   \---lib
    |   |   |   +---loader-utils
    |   |   |   |   \---lib
    |   |   |   +---pkg-dir
    |   |   |   +---schema-utils
    |   |   |   |   \---src
    |   |   |   +---serialize-javascript
    |   |   |   |   \---.vscode
    |   |   |   +---source-map
    |   |   |   |   +---dist
    |   |   |   |   \---lib
    |   |   |   +---source-map-support
    |   |   |   +---tapable
    |   |   |   |   \---lib
    |   |   |   +---terser
    |   |   |   |   +---bin
    |   |   |   |   +---dist
    |   |   |   |   \---tools
    |   |   |   +---terser-webpack-plugin
    |   |   |   |   \---dist
    |   |   |   \---webpack-sources
    |   |   |       \---lib
    |   |   +---schemas
    |   |   |   \---plugins
    |   |   |       +---debug
    |   |   |       \---optimize
    |   |   \---web_modules
    |   +---webpack-dev-middleware
    |   |   +---lib
    |   |   \---node_modules
    |   |       +---.bin
    |   |       \---mime
    |   |           \---types
    |   +---webpack-dev-server
    |   |   +---bin
    |   |   +---client
    |   |   |   +---clients
    |   |   |   \---utils
    |   |   +---lib
    |   |   |   +---servers
    |   |   |   \---utils
    |   |   +---node_modules
    |   |   |   +---.bin
    |   |   |   +---chokidar
    |   |   |   |   +---lib
    |   |   |   |   \---types
    |   |   |   +---debug
    |   |   |   |   \---src
    |   |   |   +---is-absolute-url
    |   |   |   +---ms
    |   |   |   +---normalize-path
    |   |   |   +---schema-utils
    |   |   |   |   \---src
    |   |   |   +---semver
    |   |   |   |   \---bin
    |   |   |   \---ws
    |   |   |       \---lib
    |   |   \---ssl
    |   +---webpack-log
    |   |   +---node_modules
    |   |   |   \---ansi-colors
    |   |   |       \---types
    |   |   \---src
    |   |       \---loglevel
    |   +---webpack-merge
    |   |   \---dist
    |   +---webpack-sources
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---source-map
    |   |           +---dist
    |   |           \---lib
    |   +---webpack-subresource-integrity
    |   |   \---node_modules
    |   |       +---source-map
    |   |       |   +---dist
    |   |       |   \---lib
    |   |       \---webpack-sources
    |   |           \---lib
    |   +---websocket-driver
    |   |   \---lib
    |   |       \---websocket
    |   |           \---driver
    |   |               \---hybi
    |   +---websocket-extensions
    |   |   \---lib
    |   |       \---pipeline
    |   +---whatwg-mimetype
    |   |   \---lib
    |   +---which
    |   |   \---bin
    |   +---which-module
    |   +---wide-align
    |   +---wildcard
    |   |   +---examples
    |   |   \---test
    |   +---worker-farm
    |   |   +---examples
    |   |   |   +---basic
    |   |   |   \---pi
    |   |   +---lib
    |   |   |   \---child
    |   |   \---tests
    |   +---worker-plugin
    |   |   +---dist
    |   |   +---node_modules
    |   |   |   +---.bin
    |   |   |   +---json5
    |   |   |   |   +---dist
    |   |   |   |   \---lib
    |   |   |   \---loader-utils
    |   |   |       \---lib
    |   |   \---src
    |   +---wrap-ansi
    |   +---wrappy
    |   +---ws
    |   |   \---lib
    |   +---xml2js
    |   |   \---lib
    |   +---xmlbuilder
    |   |   +---lib
    |   |   \---typings
    |   +---xtend
    |   +---y18n
    |   +---yallist
    |   +---yaml
    |   |   +---browser
    |   |   |   +---dist
    |   |   |   \---types
    |   |   +---dist
    |   |   \---types
    |   +---yargs
    |   |   +---lib
    |   |   +---locales
    |   |   \---node_modules
    |   |       +---ansi-regex
    |   |       +---get-caller-file
    |   |       +---is-fullwidth-code-point
    |   |       +---require-main-filename
    |   |       +---string-width
    |   |       \---strip-ansi
    |   +---yargs-parser
    |   |   +---lib
    |   |   \---node_modules
    |   |       \---camelcase
    |   +---yn
    |   +---yocto-queue
    |   \---zone.js
    |       +---dist
    |       \---lib
    |           +---browser
    |           +---closure
    |           +---common
    |           +---extra
    |           +---jasmine
    |           +---mix
    |           +---mocha
    |           +---node
    |           +---rxjs
    |           +---testing
    |           \---zone-spec
    \---src
        +---app
        |   +---admin
        |   +---authentication
        |   +---footer
        |   +---home
        |   +---login
        |   +---model
        |   +---navigation
        |   +---region
        |   +---register
        |   +---services
        |   +---user
        |   \---userProfile
        |       +---create-user-profile
        |       +---edit-user-profile
        |       \---view-user-profile
        +---assets
        \---environments
